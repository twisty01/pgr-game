﻿#include "LoadingScene.h"
#include "SceneManager.h"
#include "AnimatedBanner.h"
#include "CurveAnimation.h"
#include "Data.h"
#include <glm/gtx/string_cast.hpp>
#include "Input.h"
#include <glm/gtx/vector_angle.inl>
#include "ProgressBarObject.h"
#include <iostream>

void LoadingScene::init(AssetManager& am) {
	am.shader("texture", "texture.vert", "texture.frag")
	  ->initUni("texSampler")
	  ->initUni("PVM")
	  ->initUni("animated")
	  ->initUni("currentRow")
	  ->initUni("currentCol")
	  ->initUni("numRows")
	  ->initUni("numCols")
	  ->initAttr("position")
	  ->initAttr("uv");

	float w = float(SceneManager::WINDOW_WIDTH) / SceneManager::WINDOW_HEIGHT;
	Mesh* loadingMesh = am.createBanner("loading", (am.basePath + "img/loading.png").c_str(), "texture", w + 0.05f);
	Mesh* progressMesh = am.createBanner("progress", (am.basePath + "img/progress.png").c_str(), "texture", w, w, vec2(w));
	Mesh* animationMesh = am.createBanner("loading_animation", (am.basePath + "img/star.png").c_str(), "texture", w, w, vec2(w));
	Mesh* animationMesh_centered = am.createBanner("comet", (am.basePath + "img/star.png").c_str(), "texture", 0.1f, 0.1f);

	loadedTexture = am.texture("loaded", (am.basePath + "img/loaded.png").c_str());
	controlsTexture = am.texture("controls", (am.basePath + "img/controls.png").c_str());

	// ------------------------------
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, progressMesh->texture);
	float color[4] = {0.0f,0.0f,0.0f,0.0f};
	glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, color);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);
	glBindTexture(GL_TEXTURE_2D, 0);
	// ------------------------------

	splashBanner = new Object(loadingMesh);
	splashBanner->trY(0.005f);

	progressBanner = new ProgressBarObject(progressMesh);
	// ------------------------------
	uiObjects.push_back(splashBanner);
	uiObjects.push_back(progressBanner);
	// ------------------------------

	light = new AnimatedBanner(animationMesh, AnimatedBanner::R2L_T2B, 5, 4, 3.4f);
	light->trZ(1.0f);
	light->trX(0.6f);
	light->trY(0.34f);
	light->transform.scale *= 0.1f;
	// ------------------------------
	comet = new AnimatedBanner(animationMesh_centered, AnimatedBanner::R2L_T2B, 5, 4, 2.0f);
	comet->trZ(2.0f);

	test = new AnimatedBanner(animationMesh_centered, AnimatedBanner::R2L_T2B, 5, 4, 1.3f);
	test->trZ(3.0f);

	glutSetCursor(GLUT_CURSOR_CROSSHAIR);
}

float time = 0, time2 = 0;
vec3 basePos = vec3(-0.5f, -0.5f, 2.0f);
vec3 baseDir = normalize(vec3(1.0f, 1.0f, 0.0f));

vec3 curve[8] = {
	vec3(1.0f, 0.0f, 0.0f),
	vec3(0.2f, 0.0f, 0.0f),
	vec3(-0.3f, 0.0f, 0.0f),
	vec3(-0.5f, 0.0f, 0.0f),
	
	vec3(-0.2f, 0.0f, 0.0f),
	vec3(0.5f, 0.0f, 0.0f),
	vec3(1.0f, 0.0f, 0.0f),
};

void LoadingScene::update(float delta) {
	time += time > 3.0f ? 0.001f : (delta * 2.0f);
	time += delta * 0.1f;
	if (time > curveCometSize) time -= curveCometSize;

	CurveAnimation::alignObject(
		CurveAnimation::evaluateClosedCurve(curveCometData, curveCometSize, time) + basePos,
		CurveAnimation::evaluateClosedCurve_1stDerivative(curveCometData, curveCometSize, time),
		baseDir, comet->transform);

	// INPUTS
	if (loaded) {
		if (!controls && Input::SPACE_PRESSED) {
			Input::SPACE_PRESSED = false;
			setDone();
		}
		if (!controls && Input::R_PRESSED) {
			Input::R_PRESSED = false;
			reload = true;
		}
		if (Input::ESC_PRESSED) {
			Input::ESC_PRESSED = false;
			controls = !controls;
			if (controls)
				splashBanner->texture = controlsTexture;
			else
				splashBanner->texture = loadedTexture;
		}
	}
	Scene::update(delta);
}

void LoadingScene::setProgress(float progress) { progressBanner->setProgress(progress); }

void LoadingScene::finishLoading() {
	setProgress(100.0f);
	splashBanner->texture = loadedTexture;
	loaded = true;
	uiObjects.push_back(light);
	uiObjects.push_back(comet);
	uiObjects.push_back(test);
}

bool LoadingScene::shouldReload() {
	bool ret = reload;
	reload = false;
	return ret;
}
