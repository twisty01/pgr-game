#include "SceneManager.h"
#include "GameScene.h"
#include "Input.h"
#include <sstream>
#include <fstream>

int SceneManager::WINDOW_WIDTH = 0;
int SceneManager::WINDOW_HEIGHT = 0;

bool SceneManager::USE_MATERIALS = true;

SceneManager::~SceneManager() {
	currentScene = NULL;
	if (gameScene != NULL) delete gameScene;
	if (loadingScene != NULL) delete loadingScene;
}


void SceneManager::init() {
	ifstream ifs("config.json");
	stringstream strStream;
	strStream << ifs.rdbuf();
	string jsonStr = strStream.str();
	JSONValue* value = JSON::Parse(jsonStr.c_str());
	if (value == NULL || !value->IsObject()) {
		printf("Config failed to parse\n");
		return ;
	}
	JSONObject root = value->AsObject();

	JSONObject::iterator it = root.find("assets_path");
	if (it != root.end()) {
		am.basePath = it->second->AsString();
	}
	delete value;

	gameScene = new GameScene();
	currentScene = loadingScene = new LoadingScene();
	currentScene->reinit(am);
}

// ReSharper disable CppMemberFunctionMayBeConst
void SceneManager::setProgress(float progress) { loadingScene->setProgress(progress); }
void SceneManager::finishLoading() { loadingScene->finishLoading(); }

void SceneManager::update(float elapsed) {
	currentScene->update(elapsed);
	if (currentScene->isDone()) switchScenes();
	if ( currentScene == loadingScene && loadingScene->shouldReload()) {
		delete gameScene;
		gameScene = new GameScene();
		currentScene = gameScene;
		currentScene->reinit(am);
	}
}

void SceneManager::switchScenes() {
	if (currentScene == gameScene)
		currentScene = loadingScene;
	else if (currentScene == loadingScene)
		currentScene = gameScene;
	currentScene->reinit(am);
}

void SceneManager::draw() { currentScene->draw(); }

void SceneManager::keyPress(unsigned char key, int mouse_x, int mouse_y) {
	Input::setKeyPressed(key);
	currentScene->keyPress(key, mouse_x, mouse_y);
}

void SceneManager::keyUp(unsigned char key, int mouse_x, int mouse_y) {
	Input::setKeyUp(key);
	currentScene->keyUp(key, mouse_x, mouse_y);
}

void SceneManager::specialKeyPress(int specKeyPressed, int mouse_x, int mouse_y) { currentScene->specialKeyPress(specKeyPressed, mouse_x, mouse_y); }
void SceneManager::specialKeyUp(int specKeyPressed, int mouse_x, int mouse_y) { currentScene->specialKeyUp(specKeyPressed, mouse_x, mouse_y); }
void SceneManager::reshape(int w, int h) { currentScene->reshape(WINDOW_WIDTH = w, WINDOW_HEIGHT = h); }
void SceneManager::mouseMove(int x, int y) { currentScene->mouseMove(x, y); }
void SceneManager::mouseClick(int button, int state, int x, int y) {
	Input::setMouseClick(button, state);
	currentScene->mouseClick(button, state, x, y);
}

void SceneManager::mouseWheel(int wheel, int direction, int x, int y) { currentScene->mouseWheel(wheel, direction, x, y); }
void SceneManager::passiveMouseMove(int i, int y) { currentScene->passiveMouseMove(i, y); }
