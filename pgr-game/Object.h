﻿/**
* Adam Fišer 16.05.2017
*/
#pragma once

#include "Mesh.h"
#include "Transform.h"
#include "Shader.h"

/**
 * \brief Object class serves as default interface for drawables in Scene. 
 * Wraps transforms and custom shader attribs and uniforms around mesh.
 */
class Object {
public:
	Mesh* mesh;
	Shader* shader;
	Transform transform;

	GLuint texture;
	GLuint specularMap;
	GLuint normalMap;

	Material* material = NULL;

	static bool USE_FOG_GLOBAL;

	bool useFog = true;

	Object(Mesh* mesh, Shader* shader, GLuint texture) : mesh(mesh), shader(shader), texture(texture),material(mesh->material),specularMap(mesh->specularMap),normalMap(mesh->normalMap) {}
	Object(Mesh* mesh, Shader* shader) : Object(mesh, shader, mesh->texture) {}
	Object(Mesh* mesh, GLuint texture) : Object(mesh, mesh->shader, texture) {}
	Object(Mesh* mesh) : Object(mesh, mesh->shader, mesh->texture) {}
	Object() {};

	virtual ~Object() { if (material != NULL && material != mesh->material) delete material; }

	/**
	 * Updates attributes, binds vertex array objects and buffers and textue and draws mesh
	 */
	void virtual draw(Camera* camera);
	/**
	 * Updates attribs and uniforms, for more logic should be overiden.
	 */
	void virtual update(float delta);
	/**
	* Initializes objects
	*/
	void virtual init();
	/**
	* Updates shader attribs and uniforms
	*/
	virtual void updateAttribs();

	void trX(float deltaX) { transform.position.x += deltaX; }
	void trY(float deltaY) { transform.position.y += deltaY; }
	void trZ(float deltaZ) { transform.position.z += deltaZ; }

	float getX() const { return transform.position.x; }
	float getY() const { return transform.position.y; }
	float getZ() const { return transform.position.z; }

	float getW() const { return mesh->width; }
	float getH() const { return mesh->height; }
	float getD() const { return mesh->depth; }

	float getScX() const { return transform.scale.x; }
	float getScY() const { return transform.scale.y; }
	float getScZ() const { return transform.scale.z; }

};
