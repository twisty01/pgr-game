﻿/**
* Adam Fišer 16.05.2017
*/
#pragma once
#include "Light.h"

using namespace glm;

class Lights {
public:
	Light sun;
	Light lamp;
	Light flash;

	Lights() {
		sun.ambient = vec3(0.2f);
		sun.diffuse = vec3(1.5f, 1.5f, 1.0f);
		sun.specular = vec3(1.5f, 1.5f, 1.0f);
		sun.direction = normalize(vec3(-1.0f, -0.5f, 0.0f));
		sun.position = -sun.direction * 600.0f;

		lamp.ambient = vec3(1.3f, 1.0f, 1.0f);
		lamp.diffuse = vec3(2.5f, 1.9f, 1.5f);
		lamp.specular = vec3(2.5f, 1.9f, 2.0);
		lamp.attenuation = vec3(0.1f, 0.1f, 0.005f);

		flash.ambient = vec3(0.6f, 1.0f, 0.6f);
		flash.diffuse = vec3(0.8f, 1.4f, 0.8f);
		flash.specular = vec3(1.0f, 1.9f, 1.0f);
		flash.attenuation = vec3(0.0f, 0.05f, 0.003f);
		flash.spotCosCutOff = 0.93f;
		flash.spotExponent = 5.0f;
	}

	void setLightUniforms(Shader* shader, float cosSunShadow, bool computeSunShadow, vec3 planetPosition) const {
		GLint sunAmbientL = shader->get("sun.ambient"),
				sunDiffuseL = shader->get("sun.diffuse"),
				sunSpecularL = shader->get("sun.specular"),
				sunDirectionL = shader->get("sun.direction"),
				cosSunShadowL = shader->get("cosSunShadow"),
				planetCenterL = shader->get("planetCenter");

		if (sunAmbientL != -1) glUniform3fv(sunAmbientL, 1, value_ptr(sun.ambient));
		if (sunDiffuseL != -1) glUniform3fv(sunDiffuseL, 1, value_ptr(sun.diffuse));
		if (sunSpecularL != -1) glUniform3fv(sunSpecularL, 1, value_ptr(sun.specular));
		if (sunDirectionL != -1) glUniform3fv(sunDirectionL, 1, value_ptr(sun.direction));
		if (cosSunShadowL != -1) glUniform1f(cosSunShadowL, computeSunShadow ? cosSunShadow : 1.0f );
		if (planetCenterL != -1) glUniform3fv(planetCenterL, 1, value_ptr(planetPosition));

		GLint lampAbientL = shader->get("lamp.ambient"),
				lampDiffuseL = shader->get("lamp.diffuse"),
				lampSpecularL = shader->get("lamp.specular"),
				lampPositionL = shader->get("lamp.position"),
				lampAttentuationL = shader->get("lamp.attenuation");

		if (lampAbientL != -1) glUniform3fv(lampAbientL, 1, value_ptr(lamp.ambient));
		if (lampDiffuseL != -1) glUniform3fv(lampDiffuseL, 1, value_ptr(lamp.diffuse));
		if (lampSpecularL != -1) glUniform3fv(lampSpecularL, 1, value_ptr(lamp.specular));
		if (lampPositionL != -1) glUniform3fv(lampPositionL, 1, value_ptr(lamp.position));
		if (lampAttentuationL != -1) glUniform3fv(lampAttentuationL, 1, value_ptr(lamp.attenuation));

		GLint flashAmbientL = shader->get("flash.ambient"),
				flashDiffuseL = shader->get("flash.diffuse"),
				flashSpecularL = shader->get("flash.specular"),
				flashPositionL = shader->get("flash.position"),
				flashDirL = shader->get("flash.direction"),
				flashSpotExpL = shader->get("flash.spotExponent"),
				flashSpotCosCutOffL = shader->get("flash.spotCosCutOff"),
				flashAttentuationL = shader->get("flash.attenuation");

		if (flashAmbientL != -1) glUniform3fv(flashAmbientL, 1, value_ptr(flash.ambient));
		if (flashDiffuseL != -1) glUniform3fv(flashDiffuseL, 1, value_ptr(flash.diffuse));
		if (flashSpecularL != -1) glUniform3fv(flashSpecularL, 1, value_ptr(flash.specular));
		if (flashPositionL != -1) glUniform3fv(flashPositionL, 1, value_ptr(flash.position));
		if (flashAttentuationL != -1) glUniform3fv(flashAttentuationL, 1, value_ptr(flash.attenuation));
		if (flashDirL != -1)  glUniform3fv(flashDirL, 1, value_ptr(flash.direction));
		if (flashSpotExpL != -1)  glUniform1f(flashSpotExpL, flash.spotExponent);
		if (flashSpotCosCutOffL != -1)  glUniform1f(flashSpotCosCutOffL, flash.spotCosCutOff);
	}

	~Lights() {};

	
};
