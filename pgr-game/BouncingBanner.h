﻿/**
* Adam Fišer 16.05.2017
*/
#pragma once
#include "Object.h"

class BouncingBanner : public Object{
public:

	float time = 0;

	BouncingBanner(Mesh* mesh)
		: Object(mesh) {
			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, texture);
			glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
			glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
			glBindTexture(GL_TEXTURE_2D, 0);
	}

	void update(float delta) override{
		time += delta;
	}

	void updateAttribs() override{
		GLint timeL = shader->get("time");
		if (timeL != -1) glUniform1f(timeL, time);
	}
};
