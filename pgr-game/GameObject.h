/**
* Adam Fi�er 16.05.2017
*/
#pragma once
#include "Object.h"
#include "Lights.h"

class GameObject : public Object {

public:
	// sun stuff
	bool computeSunShadow = false;
	float radius;

	vec3 basePos = vec3(0.0f, 1.0f, 0.0f);
	vec3 baseDir = vec3(0.0f, 0.0f, 1.0f);

	vec3 direction = baseDir;

	float speed = 1.7f;
	float lookSpeed = 125.0f;

	bool isRigid = false;

	GameObject(Mesh* mesh, float planet_radius, bool compute_sun_shadow = true)
		: Object(mesh), computeSunShadow(compute_sun_shadow), radius(planet_radius) {
		basePos = vec3(0.0f, radius, 0.0f);
		transform.position = basePos;
	}

	void updateAttribs() override;
	void update(float delta) override;

	//-------------------------------------------
	void computePositionAndRotation();

	void move(float d);
	void turn(float d);

	void moveToPosition(vec3 newPosition);
	bool moveToPosition(vec3 newPosition, vector<GameObject*>& rigidObjects);
	void rotateInDeg(float degrees);
	
	
	float distance(const Object* object) const;
	float distance(vec3 point) const;
	bool intersects(const GameObject* o) const;

	virtual float computeBoundRadius() const;
};
