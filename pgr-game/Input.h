/**
* Adam Fi�er 16.05.2017
*/
#pragma once


class Input {
public:
	static bool MOUSE_LEFT_PRESSED;
	static bool MOUSE_RIGHT_PRESSED;
	static bool MOUSE_MIDDLE_PRESSED;

	static bool W_PRESSED;
	static bool S_PRESSED;
	static bool A_PRESSED;
	static bool D_PRESSED;
	static bool Q_PRESSED;
	static bool E_PRESSED;
	static bool SPACE_PRESSED;
	static bool ESC_PRESSED;
	static bool R_PRESSED;
	static bool T_PRESSED;

	static void setKeyPressed(unsigned char key);
	static void setKeyUp(unsigned char key);
	static void Input::setMouseClick(int button, int state);
};
