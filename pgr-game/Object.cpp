﻿#include "Object.h"
#include "Camera.h"
#include "pgr.h"
#include <iostream>


bool Object::USE_FOG_GLOBAL = true;

void Object::draw(Camera* camera) {
	glUseProgram(shader->program);
	glBindVertexArray(mesh->vao);
	updateAttribs();

	GLint PVMloc = shader->get("PVM");
	if (PVMloc != -1) {
		mat4 PVM = camera->projection * camera->view * transform.computeTransform();
		glUniformMatrix4fv(PVMloc, 1, GL_FALSE, value_ptr(PVM));
	}

	GLint Vloc = shader->get("V");
	if (Vloc != -1) glUniformMatrix4fv(Vloc, 1, GL_FALSE, value_ptr(camera->view));

	GLint Mloc = shader->get("M");
	if (Mloc != -1) glUniformMatrix4fv(Mloc, 1, GL_FALSE, value_ptr(transform.getModelMatrix()));

	//	GLint itMloc = shader->get("itM");
	//	if (itMloc != -1) {
	//		glm::mat4 itM = glm::transpose(glm::inverse(transform.model));
	//		glUniformMatrix4fv(itMloc, 1, GL_FALSE, glm::value_ptr(transform.model));
	//	}
	mesh->material->setUniforms(shader);

	GLint useFogL = shader->get("useFog");
	if (useFogL != -1) {
		glUniform1i(useFogL, useFog && USE_FOG_GLOBAL);
	}

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture);

//	GLint useSmL = shader->get("material.useSpecularMap");
//	if (useSmL != -1) {
//		glUniform1i(useSmL, material->useSpecularMap);
//		GLint smL = shader->get("specularMap");
//		if (material->useSpecularMap && smL != -1) {
//			if (specularMap != 0) {
//				glActiveTexture(GL_TEXTURE1);
//				glBindTexture(GL_TEXTURE_2D, specularMap);
//				glUniform1i(smL, 1);
//			} else glUniform1i(smL, 0);
//		}
//	}
//
//	GLint useNmL = shader->get("material.useNormalMap");
//	if (useNmL != -1) {
//		glUniform1i(useNmL, material->useNormalMap);
//		GLint nmL = shader->get("normalMap");
//		if (material->useNormalMap && nmL != -1) {
//			if (normalMap != 0) {
//				glActiveTexture(GL_TEXTURE2);
//				glBindTexture(GL_TEXTURE_2D, normalMap);
//				glUniform1i(nmL, 2);
//			} else glUniform1i(nmL, 0);
//		}
//	}


	mesh->draw();

	glUseProgram(0);
	glBindVertexArray(0);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, 0);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, 0);
}

//! To be overriden 
void Object::update(float delta) {}

//! To be overriden 
void Object::init() {}

//! To be overriden 
void Object::updateAttribs() {}
