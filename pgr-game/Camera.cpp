#include "Camera.h"


Camera::Camera(vec3 eye, vec3 center, vec3 up, int width, int height, const char type) : eye(eye), center(center), up(up), type(type) {
	switch (type) {
		case PERSPECTIVE:
			projection = perspective(35.0f, float(width) / height, 0.1f, 10000.0f);
			break;
		case ORTHO:
			projection = ortho(-1.0f * width / height, 1.0f * width / height, -1.0f, 1.0f, 0.0f, 100.0f);
			break;
	}
	lookAt(eye, center, up);
}

void Camera::reshape(int width, int height) {
	switch (type) {
		case PERSPECTIVE:
			projection = perspective(35.0f, float(width) / height, 0.1f, 10000.0f);
			break;
		case ORTHO:
			projection = ortho(-1.0f * width / height, 1.0f * width / height, -1.0f, 1.0f, 0.0f, 100.0f);
			break;
	}
}

void Camera::update() {
	if (inTransition()) {
		float k = animTime > 0.0f ? (currentTime / animTime) : 1.0f;
		eye = mix(eye, targetEye, k);
		center = mix(center, targetCenter, k);
		up = mix(up, targetUp, k);
		if (currentTime > animTime) { finishTransition(); }
	}
	view = lookAt(eye, center, up);
}

void Camera::setTransition(vec3 targetCenter, vec3 targetEye, vec3 targetUp, float animTime) {
	if (animTime > 0.0f) {
		this->targetCenter = targetCenter;
		this->targetEye = targetEye;
		this->targetUp = targetUp;
		this->animTime = animTime;
		currentTime = 0.0f;
		transition = true;
	} else {
		center = targetCenter;
		eye = targetEye;
		up = targetUp;
		transition = false;
	}
}

void Camera::finishTransition() {
	transition = false;
	eye = targetEye;
	center = targetCenter;
	up = targetUp;
}

//void Camera::computeUp(float elevationAngle) {
//	vec3 dir = center - eye;
//	vec3 rotationAxis = cross(dir, vec3(0.0f, 0.0f, 1.0f));
//	mat4 cameraTransform = glm::rotate(mat4(1.0f), elevationAngle, rotationAxis);
//	vec3 up = vec3(cameraTransform * vec4(up, 0.0f));
//}
