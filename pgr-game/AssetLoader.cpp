﻿#include "AssetLoader.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include "SceneManager.h"

string AssetLoader::basePath = "";

Material* AssetLoader::loadMaterial(JSONObject& root) {
	if (!SceneManager::USE_MATERIALS) return NULL;
	auto it = root.find("material");
	if (it == root.end())return NULL;
	Material* m = new Material();
	if (it->second->IsNumber() && bool(it->second->AsNumber()) == true) return m;

	auto& matObj = it->second->AsObject();
	auto ambientIt = matObj.find("ambient");
	if (ambientIt != matObj.end()) {
		auto arr = ambientIt->second->AsArray();
		m->ambient = vec3(arr[0]->AsNumber(), arr[1]->AsNumber(), arr[2]->AsNumber());
	}
	auto diffuseIt = matObj.find("diffuse");
	if (diffuseIt != matObj.end()) {
		auto arr = diffuseIt->second->AsArray();
		m->diffuse = vec3(arr[0]->AsNumber(), arr[1]->AsNumber(), arr[2]->AsNumber());
	}
	auto specularIt = matObj.find("specular");
	if (specularIt != matObj.end()) {
		auto arr = specularIt->second->AsArray();
		m->specular = vec3(arr[0]->AsNumber(), arr[1]->AsNumber(), arr[2]->AsNumber());
	}
	auto shininessIt = matObj.find("shininess");
	if (shininessIt != matObj.end())
		m->shininess = float(shininessIt->second->AsNumber());

	auto useTexIt = matObj.find("use_texture");
	if (useTexIt != matObj.end())
		m->useTexture = bool(useTexIt->second->AsNumber());

	auto useDiffIt = matObj.find("use_specular_map");
	if (useDiffIt != matObj.end())
		m->useSpecularMap = bool(useDiffIt->second->AsNumber());

	auto useNormalIt = matObj.find("use_normal_map");
	if (useNormalIt != matObj.end())
		m->useNormalMap = bool(useDiffIt->second->AsNumber());

	return m;
}

void AssetLoader::loadNextAnimatedMesh(AssetManager* am, JSONObject::iterator& it) {
	auto obj = it->second->AsObject();
	auto tex = obj.find("texture_file");
	Material* m = loadMaterial(obj);
	int frames = int(obj["frames"]->AsNumber());
	float time = float(obj["time"]->AsNumber());
	if (tex != obj.end())
		am->animatedMesh(it->first, basePath + obj["base_filename"]->AsString(), obj["shader"]->AsString(), (basePath + tex->second->AsString()).c_str(), m, frames, time, AnimatedMesh::LOOP);
	else if ((tex = obj.find("texture")) != obj.end())
		am->animatedMesh(it->first, basePath + obj["base_filename"]->AsString(), obj["shader"]->AsString(), am->texture(tex->second->AsString()), m, frames, time, AnimatedMesh::LOOP);
	else
		am->animatedMesh(it->first, basePath + obj["base_filename"]->AsString(), obj["shader"]->AsString(), m, frames, time, AnimatedMesh::LOOP);
}

void AssetLoader::loadNextMesh(AssetManager* am, JSONObject::iterator& it) {
	auto obj = it->second->AsObject();
	auto nmIt = obj.find("normal_map_file");
	GLuint normalMap = 0, specularMap = 0;
	if (nmIt != obj.end())
		normalMap = am->texture(nmIt->second->AsString(), nmIt->second->AsString().c_str());
	 else if ((nmIt = obj.find("normal_map")) != obj.end()) 
		 normalMap = am->texture(nmIt->second->AsString());
	
	auto spIt = obj.find("specular_map_file");
	if (spIt != obj.end())
		specularMap = am->texture(nmIt->second->AsString(), nmIt->second->AsString().c_str());
	else if ((spIt = obj.find("specular_map")) != obj.end())
		specularMap = am->texture(nmIt->second->AsString());

	Material* m = loadMaterial(obj);
	Mesh * mesh;
	auto tex = obj.find("texture_file");
	if (tex != obj.end())
		mesh = am->mesh(it->first, basePath + obj["filename"]->AsString(), obj["shader"]->AsString(), (basePath + tex->second->AsString()).c_str(), m);
	else if ((tex = obj.find("texture")) != obj.end())
		mesh = am->mesh(it->first, basePath + obj["filename"]->AsString(), obj["shader"]->AsString(), am->texture(tex->second->AsString()), m);
	else
		mesh = am->mesh(it->first, basePath + obj["filename"]->AsString(), obj["shader"]->AsString(), m);
	
	mesh->specularMap = specularMap;
	mesh->normalMap = normalMap;
}

void AssetLoader::loadNextBanner(AssetManager* am, JSONObject::iterator& it) {
	auto obj = it->second->AsObject();
	float width = float(obj["width"]->AsNumber());
	am->createBanner(it->first, (basePath + obj["filename"]->AsString()).c_str(), obj["shader"]->AsString(), width);
}

void AssetLoader::loadNextSkybox(AssetManager* am, JSONObject::iterator& it) {
	auto obj = it->second->AsObject();
	am->createSkybox(it->first, basePath + obj["base_filename"]->AsString(), obj["extension"]->AsString(), obj["shader"]->AsString());
}

void AssetLoader::loadNextTexture(AssetManager* am, JSONObject::iterator& texIt) { am->texture(texIt->first, (basePath + texIt->second->AsString()).c_str()); }

void AssetLoader::loadNextShader(AssetManager* am, JSONObject::iterator& it) {
	Shader* sh = am->shader(it->first, it->first + Shader::VERTEX_SHADER_EXTENSION, it->first + Shader::FRAGMENT_SHADER_EXTENSION);
	auto& vals = it->second->AsObject();
	auto uniforms = vals.find("uniforms");
	if (uniforms != vals.end())
		for (auto& uniform : uniforms->second->AsArray())
			sh->initUni(uniform->AsString());

	auto attribs = vals.find("attribs");
	if (attribs != vals.end())
		for (auto& attrib : attribs->second->AsArray())
			sh->initAttr(attrib->AsString());
}

int AssetLoader::loadAssets(AssetManager* am, const string& assetsFilename) {
	cout << "Loading assets..." << endl;
	ifstream ifs(assetsFilename);
	stringstream strStream;
	strStream << ifs.rdbuf();
	string jsonStr = strStream.str();
	value = JSON::Parse(jsonStr.c_str());
	if (value == NULL || !value->IsObject()) {
		printf("Assets failed to parse\n");
		return 1;
	}
	JSONObject root = value->AsObject();
	
	basePath = am->basePath;

	auto it = root.find("shaders");
	if (it != root.end()) jsons[0] = it->second->AsObject();

	it = root.find("textures");
	if (it != root.end()) jsons[1] = it->second->AsObject();

	it = root.find("banners");
	if (it != root.end()) jsons[2] = it->second->AsObject();

	it = root.find("skyboxes");
	if (it != root.end()) jsons[3] = it->second->AsObject();

	it = root.find("meshes");
	if (it != root.end()) jsons[4] = it->second->AsObject();

	it = root.find("animated_meshes");
	if (it != root.end()) jsons[5] = it->second->AsObject();

	itAsset = jsons[assetTypeIdx = 0].begin();
	int ret = 0;
	for (auto& q : jsons) ret += q.size();
	return ret;
}

bool AssetLoader::loadNextAsset(AssetManager* am) {
	while (itAsset == jsons[assetTypeIdx].end()) {
		assetTypeIdx++;
		if (assetTypeIdx >= jsons.size()) {
			jsons.clear();
			return false;
		}
		itAsset = jsons[assetTypeIdx].begin();
	}
	cout << "asset...." << assetTypeIdx << endl;
	switch (assetTypeIdx) {
		case 0:
			loadNextShader(am, itAsset);
			break;
		case 1:
			loadNextTexture(am, itAsset);
			break;
		case 2:
			loadNextBanner(am, itAsset);
			break;
		case 3:
			loadNextSkybox(am, itAsset);
			break;
		case 4:
			loadNextMesh(am, itAsset);
			break;
		case 5:
			loadNextAnimatedMesh(am, itAsset);
			break;
	}
	++itAsset;
	return true;
}
