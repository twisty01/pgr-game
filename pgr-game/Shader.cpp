#include "Shader.h"
#include "pgr.h"
#include <iostream>

using namespace std;

const char* Shader::VERTEX_SHADER_EXTENSION = ".vert";
const char* Shader:: FRAGMENT_SHADER_EXTENSION = ".frag";

Shader::Shader(const string& vertexSrc, const string& fragmentSrc) {
	GLuint shaders[] = {
		pgr::createShaderFromSource(GL_VERTEX_SHADER, vertexSrc),
		pgr::createShaderFromSource(GL_FRAGMENT_SHADER, fragmentSrc),
		0
	};
	program = pgr::createProgram(shaders);
}

Shader::Shader(const string& vertexFileName, const string& fragmentFileName, bool) {
	GLuint shaders[] = {
		pgr::createShaderFromFile(GL_VERTEX_SHADER, vertexFileName),
		pgr::createShaderFromFile(GL_FRAGMENT_SHADER, fragmentFileName),
		0
	};
	program = pgr::createProgram(shaders);
}

GLint Shader::get(const string& name) {
	unordered_map<basic_string<char>, int>::iterator it = locations.find(name);
	return it == locations.end() ? -1 : it->second;
}

GLint Shader::getDefUni(const char * name) {
	return glGetUniformLocation(program, name);
}

Shader* Shader::initAttr(const string& attribName) {
	GLint loc = glGetAttribLocation(program, attribName.c_str());
	if (loc == -1) cout << "init attrib '" << attribName << "' failed" << endl;
	locations[attribName] = loc;
	return this;
}

Shader* Shader::initUni(const string& uniformName) {
	GLint loc = glGetUniformLocation(program, uniformName.c_str());
	if (loc == -1) cout << "init uniform '" << uniformName << "' failed" << endl;
	locations[uniformName] = loc;
	return this;
}

