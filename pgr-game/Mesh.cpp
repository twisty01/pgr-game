#include "Mesh.h"
#include <pgr.h>
#include <iostream>
#include <iomanip>

Mesh::Mesh(const float* vertices, int numVertices, Shader* shader, Material* material, GLuint texture, const float* normals, const float* uvs):
	numVertices(numVertices), texture(texture), shader(shader), material(material) {
	computeBounds(vertices);
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);
	fillBufferAndInitAttribs(vertices, normals, uvs);
	glBindVertexArray(0);
	glUseProgram(0);
}

Mesh::Mesh(const float* vertices, int numVertices, const unsigned int* indices, int numIndices,
           Shader* shader, Material* material, GLuint texture, const float* normals, const float* uvs)
	: numVertices(numVertices), numIndices(numIndices), shader(shader), material(material),texture(texture) {
	computeBounds(vertices);
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);
	fillBufferAndInitAttribs(vertices, normals, uvs);
	glGenBuffers(1, &ebo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(int) * numIndices, indices, GL_STATIC_DRAW);

	glBindVertexArray(0);
	glUseProgram(0);
}


void Mesh::fillBufferAndInitAttribs(const float* vertices, const float* normals, const float* uvs) {
	glUseProgram(shader->program);

	GLint pl = shader->get("position"), nl = shader->get("normal"), ul = shader->get("uv");
	hasNormals = normals != NULL && nl != -1;
	hasUVs = uvs != NULL && ul != -1;

	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);

	glBufferData(GL_ARRAY_BUFFER, (hasNormals ? (hasUVs ? 8 : 6) : (hasUVs ? 5 : 3)) * sizeof(float) * numVertices, 0, GL_STATIC_DRAW);

	glBufferSubData(GL_ARRAY_BUFFER, 0, 3 * sizeof(float) * numVertices, vertices);
	if (hasNormals) glBufferSubData(GL_ARRAY_BUFFER, 3 * sizeof(float) * numVertices, 3 * sizeof(float) * numVertices, normals);
	if (hasUVs) glBufferSubData(GL_ARRAY_BUFFER, (hasNormals ? 6 : 3) * sizeof(float) * numVertices, 2 * sizeof(float) * numVertices, uvs);

	if (pl != -1) {
		glEnableVertexAttribArray(pl);
		glVertexAttribPointer(pl, 3, GL_FLOAT, GL_FALSE, 0, (void *)0);
	}
	if (hasNormals) {
		glEnableVertexAttribArray(nl);
		glVertexAttribPointer(nl, 3, GL_FLOAT, GL_FALSE, 0, (void *)(3 * numVertices * sizeof(float)));
	}
	if (hasUVs) {
		glEnableVertexAttribArray(ul);
		glVertexAttribPointer(ul, 2, GL_FLOAT, GL_FALSE, 0, (void *)((hasNormals ? 6 : 3) * numVertices * sizeof(float)));
	}
	if (material == NULL) return;
	if (texture == 0) material->useTexture = false;
}

Mesh::~Mesh() {
	delete material;
	if (vbo != 0) glDeleteBuffers(1, &vbo);
	if (ebo != 0) glDeleteBuffers(1, &ebo);
	glDeleteVertexArrays(1, &vao);
}

void computeMinMax(float& min, float& max, const float val) {
	if (val < min) min = val;
	if (val > max) max = val;
}

void Mesh::computeBounds(const float* vertices) {
	right = top = front = width = height = depth = /*numeric_limits<float>::min()*/ 0.0f;
	left = bottom = back = /*numeric_limits<float>::max()*/ 0.0f;
	for (int i = 0; i < numVertices * 3; i += 3) {
		float x = vertices[i], y = vertices[i + 1], z = vertices[i + 2];
		computeMinMax(left, right, x);
		computeMinMax(bottom, top, y);
		computeMinMax(front, back, z);
	}
	width = right - left;
	height = top - bottom;
	depth = back - front;
	cout << setprecision(2) << "Mesh created..." << " left:" << left << " right:" << right;
	cout << " top:" << top << " bottom:" << bottom;
	cout << " front: " << front << " back:" << back;
	cout << " | width: " << width << " height:" << height << " depth:" << depth << endl;
}

void Mesh::draw() {
	if (ebo != 0) glDrawElements(GL_TRIANGLES, numIndices, GL_UNSIGNED_INT, 0);
	else glDrawArrays(GL_TRIANGLES, 0, numVertices);
}
