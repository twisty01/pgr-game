#version 140

struct Material {          
  vec3  ambient;
  vec3  diffuse;
  vec3  specular;
  float shininess;
  int  useTexture;
  int  useSpecularMap;
  int  useNormalMap;
};

struct Light {        
  vec3  ambient;
  vec3  diffuse;
  vec3  specular;
  
  vec3  position;
  vec3  direction;
  
  float spotCosCutOff; // cosine of the spotlight's half angle
  float spotExponent;  // distribution of the light energy within the reflector's cone (center->cone's edge)

  vec3 attenuation;
};

uniform sampler2D texSampler;
uniform sampler2D specularMap;
uniform sampler2D normalMap;

uniform Material material;

uniform mat4 PVM;
uniform mat4 V;
uniform mat4 M;
uniform mat4 itM;

in vec2  _uv;
in vec3 _normal;            
in vec3 _position;

in vec3 _dir;

out vec4 outputColor;

uniform Light flash;
uniform Light lamp;
uniform Light sun;

uniform float cosSunShadow;
uniform int computeSunShadow;

uniform int useFog = 1;

vec3 directionalLight(Light light, Material material, vec3 fragPosition, vec3 fragNormal, vec3 diffuseValue, vec3 specularValue ) {
  vec3 L = normalize( (V * vec4(light.direction,0.0)).xyz );
  vec3 R = reflect(L, fragNormal);
  vec3 C = normalize(-fragPosition);
  float NdotL = max(0.0, dot(fragNormal, -L));
  float RdotV = max(0.0, dot(R, C));
  vec3 ret = material.ambient * light.ambient;
  ret += material.diffuse * light.diffuse * NdotL * diffuseValue;
  ret += material.specular * light.specular * pow(RdotV, material.shininess) * specularValue;
  return ret;
}

vec3 spotLight(Light light, Material material, vec3 fragPosition, vec3 fragNormal, vec3  diffuseValue, vec3 specularValue) {
  vec3 L = normalize((V * vec4(light.position, 1.0)).xyz - fragPosition);
  vec3 R = reflect(-L, fragNormal);
  vec3 C = normalize(-fragPosition);

  float NdotL = max(0.0, dot(fragNormal, L));
  float RdotV = max(0.0, dot(R, C));
  float spotCoef = max(0.0, dot(-L, (V * vec4(light.direction, 0.0)).xyz));
  
  vec3 ret = material.ambient * light.ambient;
  ret += material.diffuse * light.diffuse * NdotL * diffuseValue;
  ret += material.specular * light.specular * pow(RdotV, material.shininess) * specularValue;
  
  if (spotCoef < light.spotCosCutOff )
  	ret = vec3(0.0);
  else 
	ret *= pow(spotCoef, light.spotExponent);

  float d = distance(fragPosition, light.position);
  float attenuationFactor =  1.0 / (light.attenuation.x + light.attenuation.y * d + light.attenuation.z * d * d );
  ret *= attenuationFactor;
  return ret;
}

vec3 pointLight(Light light, Material material, vec3 fragPosition, vec3 fragNormal, vec3  diffuseValue, vec3 specularValue) {
  vec3 L = normalize((V * vec4(light.position, 1.0)).xyz - fragPosition);
  vec3 R = reflect(-L, fragNormal);
  vec3 C = normalize(-fragPosition);

  float NdotL = max(0.0, dot(fragNormal, L));
  float RdotV = max(0.0, dot(R, C));

  vec3 ret = material.ambient * light.ambient; 
  ret += material.diffuse * light.diffuse * NdotL * diffuseValue;
  ret += material.specular * light.specular * pow(RdotV, material.shininess) * specularValue;
  
  float d = distance(fragPosition, light.position);
  float attenuationFactor =  1.0 / (light.attenuation.x + light.attenuation.y * d + light.attenuation.z * d * d );
  ret *= attenuationFactor;
  return ret;
}

bool onSun(vec3 direction,vec3 sunDirection, float css){
	float cosA = dot( normalize(direction) , normalize(sunDirection));
	return cosA < css;
}

vec4 fog(vec4 oldColor, float density, vec4 fogColor) {
	float z = gl_FragCoord.z / gl_FragCoord.w;
	float f = clamp( exp( -density * density * z * z), 0.0f, 1.0f);
	vec4 ret = f * oldColor + ( 1.0 - f) * fogColor;
	return ret;
}

void main() {
  vec3 globalAmbientLight = vec3(0.1);
  outputColor = vec4(material.ambient * globalAmbientLight, 1.0);
  
  vec4 textureValue = texture(texSampler, _uv );
  vec3 diffuseValue = textureValue.xyz;
  vec3 specularValue = texture(specularMap, _uv ).xyz;
  vec3 nNormal = texture(normalMap, _uv ).xyz;

 if ( material.useSpecularMap == 0) specularValue = vec3(1.0);
 if ( material.useNormalMap == 0) nNormal = _normal;
 else nNormal *= _normal;
 vec3 normalValue = normalize(nNormal);

  if ( onSun(_dir, sun.direction, cosSunShadow) )	
	outputColor += directionalLight(sun, material, _position , normalValue, diffuseValue, specularValue );
  else
  	outputColor += material.ambient * sun.ambient;

  outputColor += pointLight(lamp, material, _position , normalValue, diffuseValue, specularValue );

  outputColor += spotLight(flash, material, _position , normalValue, diffuseValue, specularValue);
  
  if(material.useTexture != 0) outputColor *= textureValue;
  if (useFog == 1)  outputColor = fog(outputColor, 0.1, vec4(0.3f, 0.2f, 0.5f, 1.0f) );
}
