﻿/**
* Adam Fišer 16.05.2017
*/
#pragma once
#include "../SimpleJSON/src/JSON.h"
#include "Material.h"

using namespace glm;

class AssetManager;

class AssetLoader {
public:
	unsigned assetTypeIdx = 0;
	map<basic_string<char>, JSONValue*>::iterator itAsset;
	vector<JSONObject> jsons = vector<JSONObject>(6);
	JSONValue* value;
	static string basePath;

	static Material* loadMaterial(JSONObject& root);

	static void loadNextAnimatedMesh(AssetManager* am, JSONObject::iterator& it);

	static void loadNextMesh(AssetManager* am, JSONObject::iterator& it);

	static void loadNextBanner(AssetManager* am, JSONObject::iterator& it);

	static void loadNextSkybox(AssetManager* am, JSONObject::iterator& it);

	static void loadNextTexture(AssetManager* am, JSONObject::iterator& texIt);

	static void loadNextShader(AssetManager* am, JSONObject::iterator& it);

	int loadAssets(AssetManager* am, const string& assetsFilename);

	bool loadNextAsset(AssetManager* am);

	~AssetLoader() { if (value != NULL) delete value; }
};
