/**
 * Adam Fi�er 16.05.2017
 */
#pragma once

#include "Mesh.h"
#include "Shader.h"
#include <unordered_map>
#include "AnimatedMesh.h"
#include "AssetLoader.h"

using namespace std;

/**
 * \brief Loads assets( meshes, textures, shaders) and stores them to map.
 */
class AssetManager {
public:
	string basePath;
	AssetLoader *assetLoader;
	
	unordered_map<string, AnimatedMesh*> animatedMeshes;
	unordered_map<string, Mesh*> meshes;
	unordered_map<string, Shader*> shaders;
	unordered_map<string, GLuint> textures;

	AssetManager() {}
	~AssetManager() { dispose(); }
	void dispose();

	/**
	 * \brief Load next asset
	 * \return false, if all assets loaded. True otherwise
	 */
	bool loadNextAsset();
	/**
	* \brief Initializes asset loading
	* \return number of assets
	*/
	int loadAssets(const string& assets);
	/**
	* \brief Loads texture from file and store
	* \return loaded Texture
	*/
	GLuint texture(const string& key, const char* fileName) { return textures[key] = pgr::createTexture(fileName,true); }
	/**
	* \brief Gets stored texture
	* \return Texture
	*/
	GLuint texture(const string& key) { return textures[key]; }
	/**
	* \brief  Gets stored shader
	* \return Shader object
	*/
	Shader* shader(const string& key) { return shaders[key]; }
	/**
	* \brief  Stores shader
	* \return the stored Shader object
	*/
	Shader* shader(const string& key, Shader* shader) { return shaders[key] = shader; }
	/**
	* \brief Loads and stores shader from file
	* \return the stored Shader object
	*/
	Shader* shader(const string& key, const string& vert, const string& frag) { return shaders[key] = loadShader(vert,frag); }
	/**
	* \brief Gets stored Mesh
	* \return Mesh object
	*/
	Mesh* mesh(string key) { return meshes[key]; }
	/**
	* \brief Stores Mesh
	* \return the stored Mesh object
	*/
	Mesh* mesh(string key, Mesh* mesh) { return meshes[key] = mesh; }
	/**
	* \brief Creates mesh from c arrays and stores it
	* \return the stored Mesh object
	*/
	Mesh* mesh(const string& key, const float* verts, int numVerts, const string& shader) { return meshes[key] = new Mesh(verts, numVerts, shaders[shader]); }
	/**
	* \brief Creates mesh from c arrays and stores it
	* \return the stored Mesh object
	*/
	Mesh* mesh(const string& key, const float* verts, int numVerts, const unsigned int* faces, int numFaces, const string& shader) { return meshes[key] = new Mesh(verts, numVerts, faces, numFaces, shaders[shader]); }

	Mesh* mesh(const string& key, const string& filename, const string& shader, const char* textureFile, Material *material = NULL);
	Mesh* mesh(const string& key, const string& filename, const string& shader, GLint texture, Material *material = NULL);
	Mesh* mesh(const string& key, const string& filename, const string& shader, Material* material = NULL);

	Mesh* createBanner(const string& name, char const* filename, const string& shader, float width, float height = -1.0f, glm::vec2 pivot = glm::vec2(0.0f, 0.0f));
	Mesh* createSkybox(const string& name, const string& baseFilename, const string& extension, const string& shader);
	
	AnimatedMesh* animatedMesh(const string& key) { return animatedMeshes[key]; }
	AnimatedMesh* animatedMesh(const string& key, const string& basefilename, const string& shader, const char* textureFile, Material* material, int frames, float animTime, char animType);
	AnimatedMesh* animatedMesh(const string& key, const string& basefilename, const string& shader, Material* material, int frames, float animTime, char animType);
	AnimatedMesh* animatedMesh(const string& key, const string& basefilename, const string& shader, GLint tex, Material* material, int frames, float animTime, char animType);

	// ---------------- setLoading resources ----------------------
	static Shader* loadShader(const string& vert, const string& frag) { return new Shader(vert, frag, true); }
	static Mesh* loadOBJ_(const string& filename, Shader* shader, GLuint tex = 0, Material * material = NULL);
	static int loadOBJ_(const string& filename, vector<float>& vertices, vector<unsigned>& indices);

	static void loadOBJ(const string& filename, Shader* shader, GLuint tex, Material* material);
	static GLuint loadBMP(char const* filename);

};
