﻿/**
* Adam Fišer 16.05.2017
*/
#pragma once
#include "Object.h"

// todo shader -> cols, rows, current col, current row
class AnimatedBanner : public Object {
public:
	static const char L2R_B2T = 0, L2R_T2B = 1, R2L_B2T = 2, R2L_T2B = 3;

	char order;
	int numCols, numRows;
	int frames; // = cols * rows;
	float animationTime, currentTime = 0.0f;
	int currentCol = 0, currentRow = 0, currentFrame = 0;
	bool played = false;
	bool loop = true;

	void updateTime(float time);

	AnimatedBanner(Mesh* mesh, char order, int num_cols, int num_rows, float animation_time, bool loop = true)
		: Object(mesh),
		  order(order),
		  numCols(num_cols),
		  numRows(num_rows),
		  frames(num_cols * num_rows),
		  animationTime(animation_time),
		  loop(loop) {}


	void updateAttribs() override;
	void clearAttribs();

	void update(float delta) override;

	void draw(Camera* camera) override;

	void faceCamera(Camera* camera);
};
