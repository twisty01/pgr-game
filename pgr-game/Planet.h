#pragma once
#include "Object.h"

class Planet : public Object
{
public:
	Planet(Mesh& mesh, float radius)
		: Object(mesh),
		  radius(radius)
	{
	}

	float radius;

};

