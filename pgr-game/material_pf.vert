#version 140

in vec3 position;
in vec3 normal;
in vec2 uv;

in vec3 nextPosition;
in vec3 nextNormal;
in vec2 nextUv;

uniform int animated;
uniform float animationTime;

uniform vec3 planetCenter;

uniform mat4 PVM;
uniform mat4 V;
uniform mat4 M;
uniform mat4 itM;  

out vec3 _normal;
out vec2 _uv;
out vec3 _position;

out vec3 _dir;

void main() {
  
  vec3 vertexPosition = position;
  vec3 vertexNormal = normal;
  vec2 vertexUv = uv;
  
  if (animated == 1){
	vertexPosition = mix(position, nextPosition, animationTime);
	vertexNormal = mix(normal, nextNormal, animationTime);
	vertexUv = mix(uv, nextUv, animationTime);
  }

  gl_Position = PVM * vec4(vertexPosition, 1.0);
  _normal = (V * M * vec4(vertexNormal, 0.0) ).xyz;
  _position = (V * M * vec4(vertexPosition, 1.0)).xyz;   
  _uv = vertexUv;
  _dir = (M * vec4(vertexPosition,1.0)).xyz - planetCenter;
}