﻿#include "AnimatedMesh.h"
#include <iostream>


AnimatedMesh::AnimatedMesh(const float* vertices, int numVertices,
                           const unsigned* indices, int numIndices,
                           bool has_normals, bool has_uvs,
                           Shader* sh,
                           Material* m,
                           GLuint tex,
                           int num_frames, float animation_time, char type):
	Mesh(), numFrames(num_frames), currentFrame(0), animationTime(animation_time), currentTime(0.0f), type(type) {
	shader = sh;
	texture = tex;
	material = m;
	hasNormals = has_normals;
	hasUVs = has_uvs;
	// todo initialize
	glUseProgram(shader->program);
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, (hasNormals ? (hasUVs ? 8 : 6) : (hasUVs ? 5 : 3)) * sizeof(float) * numFrames * numVertices, 0, GL_STATIC_DRAW);

	glGenBuffers(1, &ebo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(int) * numIndices, indices, GL_STATIC_DRAW);
	updatePositionAndNormals();
	glBindVertexArray(0);
	glUseProgram(0);
}


void AnimatedMesh::updatePositionAndNormals() {
	currentFrame = int(numFrames * currentTime / animationTime) % numFrames;
	float timeForFrame = animationTime / numFrames;
	float mixTime = (currentTime - currentFrame * timeForFrame) / timeForFrame;

	GLint posLoc = shader->get("position");
	GLint normLoc = shader->get("normal");
	GLint uvLoc = shader->get("uv");
	GLint nextPosLoc = shader->get("nextPosition");
	GLint nextNormLoc = shader->get("nextNormal");
	GLint nextUvLoc = shader->get("nextUv");

	GLint animatedL = shader->get("animated");
	GLint animTimeL = shader->get("animTime");

	glUniform1i(animatedL, true);
	glUniform1f(animTimeL, mixTime);

	int offset = 3;
	if (hasNormals) offset += 3;
	if (hasUVs) offset += 2;

	glVertexAttribPointer(posLoc, 3, GL_FLOAT, GL_FALSE, 0, (void *)(offset * currentFrame * numVertices * sizeof(float)));
	glVertexAttribPointer(nextPosLoc, 3, GL_FLOAT, GL_FALSE, 0, (void *)(offset * ((currentFrame + 1) % numFrames) * numVertices * sizeof(float)));

	if (hasNormals) {
		glVertexAttribPointer(normLoc, 3, GL_FLOAT, GL_FALSE, 0, (void *)((offset * currentFrame * + 3) * numVertices * sizeof(float)));
		glVertexAttribPointer(nextNormLoc, 3, GL_FLOAT, GL_FALSE, 0, (void *)((offset * ((currentFrame + 1) % numFrames) + 3) * numVertices * sizeof(float)));
	}

	if (hasUVs) {
		glVertexAttribPointer(uvLoc, 2, GL_FLOAT, GL_FALSE, 0, (void *)((offset * currentFrame * + offset - 2) * numVertices * sizeof(float)));
		glVertexAttribPointer(nextUvLoc, 2, GL_FLOAT, GL_FALSE, 0, (void *)((offset * ((currentFrame + 1) % numFrames) + offset - 2) * numVertices * sizeof(float)));
	}
	CHECK_GL_ERROR();
}

void AnimatedMesh::draw() {
	updatePositionAndNormals();
	Mesh::draw();
}
