#include "Input.h"
#include <GL/freeglut_std.h>
#include <iostream>


bool Input::MOUSE_LEFT_PRESSED = false;
bool Input::MOUSE_RIGHT_PRESSED = false;
bool Input::MOUSE_MIDDLE_PRESSED = false;

bool Input::W_PRESSED = false;
bool Input::S_PRESSED = false;
bool Input::A_PRESSED = false;
bool Input::D_PRESSED = false;
bool Input::Q_PRESSED = false;
bool Input::E_PRESSED = false;
bool Input::SPACE_PRESSED = false;
bool Input::ESC_PRESSED = false;
bool Input::R_PRESSED = false;
bool Input::T_PRESSED = false;

void Input::setKeyPressed(unsigned char key) {
	//	std::cout << "key down : " << key << std::endl;
	switch (key) {
		case 'a':
			A_PRESSED = true;
			break;
		case 'd':
			D_PRESSED = true;
			break;
		case 's':
			S_PRESSED = true;
			break;
		case 'w':
			W_PRESSED = true;
			break;
		case 'q':
			Q_PRESSED = true;
			break;
		case 'e':
			E_PRESSED = true;
			break;
		case 'r':
			R_PRESSED = true;
			break;
		case 't':
			T_PRESSED = true;
			break;
		case 32:
			SPACE_PRESSED = true;
			break;
		case 27:
			ESC_PRESSED = true;
			break;
	}
}

void Input::setKeyUp(unsigned char key) {
	//	std::cout << "key up : " << key << std::endl;
	switch (key) {
		case 'a':
			A_PRESSED = false;
			break;
		case 'd':
			D_PRESSED = false;
			break;
		case 's':
			S_PRESSED = false;
			break;
		case 'w':
			W_PRESSED = false;
			break;
		case 'q':
			Q_PRESSED = false;
			break;
		case 'e':
			E_PRESSED = false;
			break;
		case 'r':
			R_PRESSED = false;
			break;
		case 't':
			T_PRESSED = false;
			break;
		case 32:
			SPACE_PRESSED = false;
			break;
		case 27:
			ESC_PRESSED = false;
			break;
	}
}

void Input::setMouseClick(int button, int state) {
	if (button == GLUT_LEFT_BUTTON) MOUSE_LEFT_PRESSED = state == GLUT_DOWN;
	else if (button == GLUT_RIGHT_BUTTON) MOUSE_RIGHT_PRESSED = state == GLUT_DOWN;
	else if (button == GLUT_MIDDLE_BUTTON) MOUSE_MIDDLE_PRESSED = state == GLUT_DOWN;
}
