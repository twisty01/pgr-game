﻿/**
* Adam Fišer 16.05.2017
*/
#pragma once
#include "GameObject.h"
#include "PrinceObject.h"

class BaobabObject : public GameObject {
public:

	const float initialBaobabRadiusOffset = 0.15f;
	const float maxGrow = 3.3f;
	const float minGrow = 0.3f;

	const float baseDamage = 0.12f;

	// ------------------------------

	static const int maxCount;
	static const float minSpawnTime;
	static const float maxSpawnTime;

	float health = 100.0f;

	static int count;
	static float SpawnTimeBound;
	static float currentSpawnTime;

	float grow = 0.1f;

	GLubyte id = 0;

	static bool shouldSpawn(float delta);
	static void resetSpawnTime();

	BaobabObject(Mesh* mesh, float planet_radius);

	void recieveDamage(float dmg);

	float damageToPlanet() const;

	// todo stencil
	void draw(Camera* camera) override;

	void update(float delta) override;

	float computeBoundRadius() const override { return GameObject::computeBoundRadius() * 0.4f; }

	void computeCollision(PrinceObject* prince) const;
	void respawn(vector<GameObject*>& rigidObjects, GLuint luint, float planet_radius);

};
