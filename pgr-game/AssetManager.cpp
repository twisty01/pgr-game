#include "AssetManager.h"
#include <fstream>
#include <sstream>
#include "pgr.h"
#include <iostream>
#include "Shader.h"
#include "Data.h"
#include <map>
#include "AnimatedMesh.h"
#include <iomanip>

using namespace std;

void AssetManager::dispose() {
	for (auto& it : meshes)  delete it.second; 
	meshes.clear();
	for (auto& it : shaders)  delete it.second; 
	shaders.clear();
	for (auto& it : textures) 	glDeleteTextures(1, &it.second);
	textures.clear();
}

int AssetManager::loadAssets(const string& assetsFilename) {
	assetLoader = new AssetLoader();
	return assetLoader->loadAssets(this, assetsFilename);
}

bool AssetManager::loadNextAsset() {
	bool ret = assetLoader->loadNextAsset(this);
	if (!ret) delete assetLoader;
	return ret;
}

// MESH WITHOUT TEXTURE
Mesh* AssetManager::mesh(const string& key, const string& filename, const string& shader, Material* material) { return meshes[key] = loadOBJ_(filename, shaders[shader], 0, material); }
// MESH WITH TEXTURE
Mesh* AssetManager::mesh(const string& key, const string& filename, const string& shader, const char* textureFile, Material* material) {
	GLuint tex = textures[textureFile] = pgr::createTexture(textureFile, true);
	return meshes[key] = loadOBJ_(filename, shaders[shader], tex, material);
}

Mesh* AssetManager::mesh(const string& key, const string& filename, const string& shader, GLint texture, Material* material) { return meshes[key] = loadOBJ_(filename, shaders[shader], texture, material); }

// ANIMATED MESH
string getFileName(const string& basefilename, int num, const string& extension) {
	stringstream s;
	s << setfill('0') << setw(6) << num;
	return basefilename + s.str() + extension;
}

AnimatedMesh* AssetManager::animatedMesh(const string& key, const string& basefilename, const string& shader, GLint tex, Material* material, int frames, float animTime, char animType) {
	vector<float> vertices, normals, uvs;
	int numVerticesNormsUvs = 0;
	vector<unsigned int> indices;
	int numIndices = 0;
	bool hasUVs = true, hasNormals = true;
	for (int i = 1; i <= frames; i++) {
		int k = loadOBJ_(getFileName(basefilename, i, ".obj"), vertices, indices);
		if (!k) {
			cout << "ERROR loading animated mesh" << endl;
			return NULL;
		}
		if (i == 1) {
			numVerticesNormsUvs = k;
			numIndices = indices.size() / 3;
			if (vertices.size() == 3 * k) {
				cout << "no normals no uvs" << endl;
				hasUVs = false;
				hasNormals = false;
			} else if (vertices.size() == k * 6)
				hasUVs = false;
			else if (vertices.size() == k * 5)
				hasNormals = false;
			else if (vertices.size() != k * 8) {
				cout << "ERROR loading animated mesh -- somethings wrong " << vertices.size() << "  vs  " << k * 8 << endl;
				return NULL;
			}
		}
	}
	cout << "vertices:" << vertices.size() << "  numVertices:" << numVerticesNormsUvs << "    indices:" << numIndices << endl;
	AnimatedMesh* m = new AnimatedMesh(&vertices[0], numVerticesNormsUvs, &indices[0], numIndices, hasNormals, hasUVs, shaders[shader], material, tex, frames, animTime, animType);
	animatedMeshes[key] = m;
	return m;
}

AnimatedMesh* AssetManager::animatedMesh(const string& key, const string& basefilename, const string& shader, const char* textureFile, Material* material, int frames, float animTime, char animType) {
	GLuint tex = textures[textureFile] = pgr::createTexture(textureFile, true);
	return animatedMesh(key, basefilename, shader, tex, material, frames, animTime, animType);
}

AnimatedMesh* AssetManager::animatedMesh(const string& key, const string& basefilename, const string& shader, Material* material, int frames, float animTime, char animType) { return animatedMesh(key, basefilename, shader, 0, material, frames, animTime, animType); }


// ------------------------ LOADING FROM FILES

Mesh* AssetManager::loadOBJ_(const string& filename, Shader* shader, GLuint tex, Material* material) {
	Assimp::Importer importer;
	importer.SetPropertyInteger(AI_CONFIG_PP_PTV_NORMALIZE, 1); // Unitize object in size (scale the model to fit into (-1..1)^3)
	const aiScene* scn = importer.ReadFile(filename.c_str(), 0
	                                       | aiProcess_Triangulate // Triangulate polygons (if any).
	                                       | aiProcess_PreTransformVertices // Transforms scene hierarchy into one root with geometry-leafs only. For more see Doc.
	                                       | aiProcess_GenSmoothNormals // Calculate normals per vertex.
	                                       | aiProcess_JoinIdenticalVertices);
	if (scn == NULL) {
		cout << "Assimp error: " << importer.GetErrorString() << endl;
		return NULL;
	}
	const aiMesh* mesh = scn->mMeshes[0];

	vector<unsigned int> indices(mesh->mNumFaces * 3);
	for (unsigned int idx = 0; idx < mesh->mNumFaces; ++idx) {
		indices[3 * idx + 0] = mesh->mFaces[idx].mIndices[0];
		indices[3 * idx + 1] = mesh->mFaces[idx].mIndices[1];
		indices[3 * idx + 2] = mesh->mFaces[idx].mIndices[2];
	}

	float* verticesptr = &mesh->mVertices[0][0];

	float* normalsptr = NULL;
	if (mesh->HasNormals()) normalsptr = &mesh->mNormals[0][0];

	vector<float> uvs(mesh->mNumVertices * 2);
	float* uvsptr = NULL;
	if (mesh->HasTextureCoords(0)) {
		for (unsigned int idx = 0; idx < mesh->mNumVertices; idx++) {
			aiVector3D uv = mesh->mTextureCoords[0][idx];
			uvs[2 * idx + 0] = uv.x;
			uvs[2 * idx + 1] = uv.y;
		}
		uvsptr = &uvs[0];
	}
	Mesh* m = new Mesh(verticesptr, mesh->mNumVertices, &indices[0], indices.size(), shader, material, tex, normalsptr, uvsptr);
	return m;
}

int AssetManager::loadOBJ_(const string& filename, vector<float>& vertices, vector<unsigned int>& indices) {
	Assimp::Importer importer;
	importer.SetPropertyInteger(AI_CONFIG_PP_PTV_NORMALIZE, 1); // Unitize object in size (scale the model to fit into (-1..1)^3)
	const aiScene* scn = importer.ReadFile(filename.c_str(), 0
	                                       | aiProcess_Triangulate // Triangulate polygons (if any).
	                                       | aiProcess_PreTransformVertices // Transforms scene hierarchy into one root with geometry-leafs only. For more see Doc.
	                                       | aiProcess_GenSmoothNormals // Calculate normals per vertex.
	                                       | aiProcess_JoinIdenticalVertices);
	if (scn == NULL) {
		cout << "Assimp error: " << importer.GetErrorString() << endl;
		return 0;
	}
	const aiMesh* mesh = scn->mMeshes[0];

	for (unsigned int idx = 0; idx < mesh->mNumFaces; ++idx) {
		indices.push_back(mesh->mFaces[idx].mIndices[0]);
		indices.push_back(mesh->mFaces[idx].mIndices[1]);
		indices.push_back(mesh->mFaces[idx].mIndices[2]);
	}
	for (unsigned int idx = 0; idx < mesh->mNumVertices; ++idx) {
		vertices.push_back(mesh->mVertices[idx].x);
		vertices.push_back(mesh->mVertices[idx].y);
		vertices.push_back(mesh->mVertices[idx].z);
	}
	if (mesh->HasNormals())
		for (unsigned int idx = 0; idx < mesh->mNumVertices; ++idx) {
			vertices.push_back(mesh->mNormals[idx].x);
			vertices.push_back(mesh->mNormals[idx].y);
			vertices.push_back(mesh->mNormals[idx].z);
		}
	if (mesh->HasTextureCoords(0)) {
		for (unsigned int idx = 0; idx < mesh->mNumVertices; idx++) {
			aiVector3D uv = mesh->mTextureCoords[0][idx];
			vertices.push_back(uv.x);
			vertices.push_back(uv.y);
		}
	}
	return mesh->mNumVertices;
}

// -----------------------------------------------------------------------------------

void AssetManager::loadOBJ(const string& filename, Shader* shader, GLuint tex, Material* material) {
	ifstream ifs(filename);
	if (!ifs.is_open()) {
		printf("Impossible to open the file !\n");
		return;
	}
	float maxCoord = 0.0f;
	string line;
	while (getline(ifs, line) && !ifs.eof()) {
		if (line.substr(0, 1) == "v") { //glm::vec3 vert;
		} else if (line.substr(0, 1) == "vt") { //glm::vec2 uv;
		} else if (line.substr(0, 1) == "vn") { //glm::vec3 normal;
		} else if (line.substr(0, 1) == "f") {
			//int face[9];
			// split by ' ' then each by '/'
		}
	}
}

// ------------------------ SHORTCUTS
/**
* Shortcut for creating banners with texture size, adds mesh and
*/
Mesh* AssetManager::createBanner(const string& name, char const* filename, const string& shader, float width, float height, vec2 pivot) {
	GLuint tex = textures[filename] = pgr::createTexture(filename, true);
	float w, h;
	glBindTexture(GL_TEXTURE_2D, tex);
	CHECK_GL_ERROR();
	glGetTexLevelParameterfv(GL_TEXTURE_2D, 0, GL_TEXTURE_WIDTH, &w);
	glGetTexLevelParameterfv(GL_TEXTURE_2D, 0, GL_TEXTURE_HEIGHT, &h);
	CHECK_GL_ERROR();
	if (width <= 0) width = w;
	float aspect = w / h;
	float rect[6 * 3];
	for (int i = 0; i < 6; i++) {
		rect[i * 3 + 0] = square_verts[i * 3 + 0] * width + pivot.x;
		rect[i * 3 + 1] = square_verts[i * 3 + 1] * (height <= 0.0f ? (width / aspect) : height) + pivot.y;
		rect[i * 3 + 2] = 0;
	}
	return meshes[name] = new Mesh(rect, 6, shaders[shader], tex, NULL, square_uvs);
}

Mesh* AssetManager::createSkybox(const string& key, const string& baseFilename, const string& extension, const string& shader) {
	Mesh* m = new Mesh();
	m->shader = shaders[shader];
	glGenVertexArrays(1, &m->vao);
	glBindVertexArray(m->vao);
	glGenBuffers(1, &m->vbo);
	glBindBuffer(GL_ARRAY_BUFFER, m->vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 8, screenCoords, GL_DYNAMIC_DRAW);

	// handles to vertex shader inputs
	glUseProgram(m->shader->program);
	GLint farplaneSceenCoordLoc = m->shader->get("uv");
	glEnableVertexAttribArray(farplaneSceenCoordLoc);
	glVertexAttribPointer(farplaneSceenCoordLoc, 2, GL_FLOAT, GL_FALSE, 0, 0);

	GLint skyboxLoc = m->shader->get("skybox");
	glUniform1i(skyboxLoc, 0);

	glActiveTexture(GL_TEXTURE0);

	glGenTextures(1, &m->texture);
	glBindTexture(GL_TEXTURE_CUBE_MAP, m->texture);

	const char* suffixes[] = {"right", "left", "bottom", "top", "back", "front"};
	GLuint targets[] = {
		GL_TEXTURE_CUBE_MAP_POSITIVE_X, GL_TEXTURE_CUBE_MAP_NEGATIVE_X,
		GL_TEXTURE_CUBE_MAP_POSITIVE_Y, GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,
		GL_TEXTURE_CUBE_MAP_POSITIVE_Z, GL_TEXTURE_CUBE_MAP_NEGATIVE_Z
	};

	for (int i = 0; i < 6; i++) {
		string texName = string(baseFilename) + suffixes[i] + '.' + extension;
		if (!pgr::loadTexImage2D(texName, targets[i])) {
			cerr << __FUNCTION__ << " cannot setProgress image " << texName << endl;
			return NULL;
		}
	}

	glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	glGenerateMipmap(GL_TEXTURE_CUBE_MAP);


	glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
	CHECK_GL_ERROR();
	return meshes[key] = m;
}

GLuint AssetManager::loadBMP(char const* filename) {
	// Data read from the header of the BMP file
	unsigned char header[54]; // Each BMP file begins by a 54-bytes header
	FILE* file = fopen(filename, "rb");
	if (!file) {
		printf("Image could not be opened\n");
		return 0;
	}
	if (fread(header, 1, 54, file) != 54) { // If not 54 bytes read : problem
		printf("Not a correct BMP file\n");
		return false;
	}
	if (header[0] != 'B' || header[1] != 'M') {
		printf("Not a correct BMP file\n");
		return 0;
	}
	// Data read from the header of the BMP file
	unsigned int dataPos = *(int*)&(header[0x0A]); // Position in the file where the actual data begins
	unsigned int width = *(int*)&(header[0x12]), height = *(int*)&(header[0x16]);
	unsigned int imageSize = *(int*)&(header[0x22]); // = width*height*3
	// Actual RGB data
	// Some BMP files are misformatted, guess missing information
	if (imageSize == 0) imageSize = width * height * 3; // 3 : one byte for each Red, Green and Blue component
	if (dataPos == 0) dataPos = 54; // The BMP header is done that way
	unsigned char* data = new unsigned char[imageSize];
	fread(data, 1, imageSize, file);
	fclose(file);
	// Create one OpenGL texture
	GLuint textureID;
	glGenTextures(1, &textureID);
	// "Bind" the newly created texture : all future texture functions will modify this texture
	glBindTexture(GL_TEXTURE_2D, textureID);
	// Give the image to OpenGL
	glTexImage2D(GL_TEXTURE_2D, 0, GL_BGRA, width, height, 0, GL_BGRA, GL_UNSIGNED_BYTE, data);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	// When MAGnifying the image (no bigger mipmap available), use LINEAR filtering
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	// When MINifying the image, use a LINEAR blend of two mipmaps, each filtered LINEARLY too
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);

	// Generate mipmaps, by the way.
	glGenerateMipmap(GL_TEXTURE_2D);
	delete[] data;
	return textureID;
}

//GLuint AssetManager::loadMTL(char const* baseFilename) { return 0; }

// todo probably not supported, do not use it
//#define FOURCC_DXT1 0x31545844 // Equivalent to "DXT1" in ASCII
//#define FOURCC_DXT3 0x33545844 // Equivalent to "DXT3" in ASCII
//#define FOURCC_DXT5 0x35545844 // Equivalent to "DXT5" in ASCII
//#define GL_COMPRESSED_RGBA_S3TC_DXT1_EXT 33777
//#define GL_COMPRESSED_RGBA_S3TC_DXT3_EXT 33778
//#define GL_COMPRESSED_RGBA_S3TC_DXT5_EXT 33779

//GLuint AssetManager::loadDDS(const char* imagepath)
//{
//	unsigned char header[124];
//	FILE* fp;
//	/* try to open the file */
//	fp = fopen(imagepath, "rb");
//	if (fp == NULL)
//		return 0;
//	/* verify the type of file */
//	char filecode[4];
//	fread(filecode, 1, 4, fp);
//	if (strncmp(filecode, "DDS ", 4) != 0)
//	{
//		fclose(fp);
//		return 0;
//	}
//	/* get the surface desc */
//	fread(&header, 124, 1, fp);
//	unsigned int height = *(unsigned int*)&(header[8]);
//	unsigned int width = *(unsigned int*)&(header[12]);
//	unsigned int linearSize = *(unsigned int*)&(header[16]);
//	unsigned int mipMapCount = *(unsigned int*)&(header[24]);
//	unsigned int fourCC = *(unsigned int*)&(header[80]);
//	//After the header is the actual data : all the mipmap levels, successively.We can read them all in one batch :
//	unsigned char* buffer;
//	unsigned int bufsize;
//	/* how big is it going to be including all mipmaps? */
//	bufsize = mipMapCount > 1 ? linearSize * 2 : linearSize;
//	buffer = (unsigned char*)malloc(bufsize * sizeof(unsigned char));
//	fread(buffer, 1, bufsize, fp);
//	/* close the file pointer */
//	fclose(fp);
//	//Here we�ll deal with 3 different formats : DXT1, DXT3 and DXT5.We need to convert the �fourCC� flag into a value that OpenGL understands.
//	GL_COMPRESSED_RGBA;
//	unsigned int components = (fourCC == FOURCC_DXT1) ? 3 : 4;
//	unsigned int format;
//	switch (fourCC)
//	{
//	case FOURCC_DXT1:
//		format = GL_COMPRESSED_RGBA_S3TC_DXT1_EXT;
//		break;
//	case FOURCC_DXT3:
//		format = GL_COMPRESSED_RGBA_S3TC_DXT3_EXT;
//		break;
//	case FOURCC_DXT5:
//		format = GL_COMPRESSED_RGBA_S3TC_DXT5_EXT;
//		break;
//	default:
//		free(buffer);
//		return 0;
//	}
//
//	//Creating the texture is done as usual :
//	// Create one OpenGL texture
//	GLuint textureID;
//	glGenTextures(1, &textureID);
//
//	// "Bind" the newly created texture : all future texture functions will modify this texture
//	glBindTexture(GL_TEXTURE_2D, textureID);
//	//And now, we just have to fill each mipmap one after another :
//
//	unsigned int blockSize = (format == GL_COMPRESSED_RGBA_S3TC_DXT1_EXT) ? 8 : 16;
//	unsigned int offset = 0;
//
//	/* setProgress the mipmaps */
//	for (unsigned int level = 0; level < mipMapCount && (width || height); ++level)
//	{
//		unsigned int size = ((width + 3) / 4) * ((height + 3) / 4) * blockSize;
//		glCompressedTexImage2D(GL_TEXTURE_2D, level, format, width, height,  0, size, buffer + offset);
//		offset += size;
//		width /= 2;
//		height /= 2;
//	}
//	free(buffer);
//
//	return textureID;
//}
