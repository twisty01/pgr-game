#version 140

in vec2 _uv;
uniform sampler2D texSampler;

out vec4 outputColor;

void main() {
    outputColor = texture( texSampler, _uv );
}