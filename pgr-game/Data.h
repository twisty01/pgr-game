/**
* Adam Fi�er 16.05.2017
*/
#pragma once

// SKYBOX
static const float screenCoords[] = {
	-1.0f, -1.0f,
	1.0f, -1.0f,
	-1.0f, 1.0f,
	1.0f, 1.0f
};

const static float diamond_verts[] = {
	0.0f, 1.0f, 0.0f,
	0.0f,-1.0f, 0.0f,

	1.0f, 0.0f, 0.0f,
	-1.0f,0.0f, 0.0f,

	0.0f, 0.0f, 1.0f,
	0.0f, 0.0f,-1.0f
};
const static unsigned int diamond_idx[] = {
	0,4,2,
	0,2,5,
	0,5,3,
	0,3,4,

	1,4,2,
	1,2,5,
	1,5,3,
	1,3,4,
};

static const float square_verts[] = {
	-1.0f , - 1.0f , 0.0f,
	1.0f , -1.0f , 0.0f,
	-1.0f , 1.0f , 0.0f,

	-1.0f , 1.0f , 0.0f,
	1.0f , -1.0f , 0.0f,
	1.0f , 1.0f , 0.0f,
};

static const float square_uvs[] = {
	0.0f, 0.0f,
	1.0f, 0.0f,
	0.0f, 1.0f,

	0.0f, 1.0f,
	1.0f, 0.0f,
	1.0f, 1.0f,
};

static const float box_elem_verts[] = {
	0.5f, 0.5f, 0.5f, // front top right
	-0.5f, 0.5f, 0.5f,// front top left
	
	0.5f, -0.5f, 0.5f,// front bottom right
	-0.5f, -0.5f, 0.5f,// front bottom left
	
	0.5f, 0.5f, -0.5f,// back top right
	-0.5f, 0.5f, -0.5f,// back top left
	
	0.5f, -0.5f, -0.5f,// back bottom right
	-0.5f, -0.5f, -0.5f,// back bottom left
};
static const unsigned int box_elem_indices[] = {
	/*front*/
	0,1,2,
	2,3,0,
	/*back*/
	4,5,6,
	6,7,4,
	/*top*/
	0,1,4,
	4,5,0,
	/*bottom*/
	3,4,6,
	6,7,3,
	/*left*/
	1,3,5,
	5,7,1,
	/*right*/
	0,2,4,
	4,6,0
};
 
using namespace glm;

static const size_t  curveEightSize = 12;
static const vec3 curveEightData[] = {
	vec3(0.00, 0.0,  0.0),

	vec3(-0.33,  0.35, 0.0),
	vec3(-0.66,  0.35, 0.0),
	vec3(-1.00,  0.0, 0.0),
	vec3(-0.66, -0.35, 0.0),
	vec3(-0.33, -0.35, 0.0),

	vec3(0.00,  0.0, 0.0),

	vec3(0.33,  0.35, 0.0),
	vec3(0.66,  0.35, 0.0),
	vec3(1.00,  0.0, 0.0),
	vec3(0.66, -0.35, 0.0),
	vec3(0.33, -0.35, 0.0)
};


static const size_t  curveCometSize = 5;
static const vec3 curveCometData[] = {
	vec3(2.6f,1.2f,0.0f),

	vec3(0.2f,0.85f,0.0f),

	vec3(-1.5f,-1.0f,0.0f),

	vec3(0.8f,-2.1f,0.0f),
	
	vec3(1.8f,-2.1f,0.0f),

};

