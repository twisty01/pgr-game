/**
* Adam Fi�er 16.05.2017
*/
#pragma once
#include <gl_core_4_4.h>
#include "Shader.h"
#include <glm/glm.hpp>
#include "Material.h"

class Camera;

class Mesh {
public:
	GLuint vao = 0, vbo = 0, ebo = 0;

	int numVertices, numIndices;

	Shader* shader = NULL;
	Material* material = NULL;
	GLuint texture = 0;
	GLuint specularMap = 0;
	GLuint normalMap = 0;

	// initialized in computeBounds
	float width, height, depth, top, left, bottom, right, front, back;
	
	bool hasNormals, hasUVs;
	Mesh() {};

	Mesh(const float* vertices, int numVertices, Shader* shader, GLuint texture = 0, const float* normals = NULL, const float* uvs = NULL)
		: Mesh(vertices, numVertices, shader, NULL, texture, normals, uvs) {}

	Mesh(const float* vertices, int numVertices, const unsigned* indices, int numIndices, Shader* shader, GLuint texture = 0, const float* normals = NULL, const float* uvs = NULL)
		: Mesh(vertices, numVertices, indices, numIndices, shader,NULL, texture, normals, uvs) {}

	Mesh(const float* vertices, int numVertices,
	     Shader* shader, Material* material, GLuint texture = 0, const float* normals = NULL, const float* uvs = NULL);

	Mesh(const float* vertices, int numVertices, const unsigned* indices, int numIndices,
	     Shader* shader, Material* material, GLuint texture = 0, const float* normals = NULL, const float* uvs = NULL);

	void fillBufferAndInitAttribs(const float* vertices, const float* normals, const float* uvs);
	virtual ~Mesh();

	virtual void draw();
	void computeBounds(const float* vertices);
};


class Builder {
	float* verts, *norms,*uvs;
	unsigned* inds;
	int numVerts, numInds;
	Shader* sh;
	GLuint tex;
	Material* mat;
public:
	void clear() {
		verts = NULL;
		numVerts = 0;
		inds = NULL;
		numInds = 0;
		sh = NULL;
		tex = 0;
		norms = NULL;
		uvs = NULL;
		mat = NULL;
	}

	Builder() { clear(); }

	Builder* vertices(const float* vertices, int numVertices) {
		verts = const_cast<float*>(vertices);
		numVerts = numVertices;
		return this;
	}

	Builder* shader(Shader* shader) { sh = shader; return this; }
	Builder* indices(const unsigned* indices, int numIndices) { inds = const_cast<unsigned*>(indices); return this;}
	Builder* normals(const float* normals) { norms = const_cast<float*>(normals); return this;}
	Builder* UVs(const float* UVs) { uvs = const_cast<float*>(UVs); return this;}
	Builder* texture(GLuint texture) { tex = texture; return this;}
	Builder* material(Material* material) { mat = material; return this;}

	Mesh* getMesh() {
		Mesh *m;
		if (inds == NULL || numInds == 0)
			m = new Mesh(verts, numVerts, sh, mat, tex, norms, uvs);
		else m = new Mesh(verts, numVerts, inds, numInds, sh, mat, tex, norms, uvs);
		clear();
		return m;
	}
};
