#version 140

uniform int currentRow;

uniform int currentCol;

uniform int numRows;

uniform int numCols;

uniform int animated;

uniform mat4 PVM;

in vec2 uv;
in vec3 position;
out vec2 _uv;

void main() {
	gl_Position = PVM * vec4(position, 1.0);
	
	if (animated == 0)  _uv = uv;
	else			_uv = vec2(currentCol * 1.0 / numCols + uv.x / numCols, currentRow * 1.0 / numRows + uv.y / numRows);
}