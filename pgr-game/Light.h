﻿/**
* Adam Fišer 16.05.2017
*/
#pragma once

class Light {
public:
	vec3  ambient; 
	vec3  diffuse;       
	vec3  specular;      
	vec3  position;      
	vec3  direction; 
	float spotCosCutOff; // cosine of the spotlight's half angle
	float spotExponent;  // distribution of the light energy within the reflector's cone (center->cone's edge)

	vec3  attenuation;
};
