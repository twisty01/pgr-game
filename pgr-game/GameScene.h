﻿/**
* Adam Fišer 16.05.2017
*/
#pragma once
#include "Scene.h"
#include "GameObject.h"
#include "PlanetObject.h"
#include "PrinceObject.h"
#include "Lights.h"
#include "ProgressBarObject.h"
#include "BaobabObject.h"
#include "AnimatedBanner.h"
#include <queue>


class ToolObject;

class GameScene : public Scene {
	// camera static positions
	// center = [0,0,0]
	const vec3 front_eye = vec3(0.0f, 0.0f, 30.0f), front_up = vec3(0.0f, 1.0f, 0.0f);
	const vec3 back_eye = vec3(0.0f, 0.0f, -30.0f), back_up = vec3(0.0f, 1.0f, 0.0f);
	const vec3 left_eye = vec3(-30.0f, 0.0f, 0.0f), left_up = vec3(0.0f, 1.0f, 0.0f);
	const vec3 right_eye = vec3(30.0f, 0.0f, 0.0f), right_up = vec3(0.0f, 1.0f, 0.0f);
	const vec3 bottom_eye = vec3(0.0f, -30.0f, 0.0f), bottom_up = vec3(0.0f, 0.0f, -1.0f);
	const vec3 top_eye = vec3(0.0f, 30.0f, 0.0f), top_up = vec3(0.0f, 0.0f, -1.0f);
	// --------------------
	int minPlanets = 3;
	int maxPlanets = 12;
	// ---------------------
	// camera switch
	bool POV = false;
	float cameraDistance;
	vec3 lastEye, lastCenter, lastUp;

	// ----------------------------------
	//pgr::CQuaternionTrackball trackball;			// trackball class -> uses quaterninons to rotate the scene
	//pgr::CClassicTrackball trackball;			// trackball class -> implementation without quaternions
	int startGrabX, startGrabY; // trackball starting point
	int endGrabX, endGrabY; // trackball end point
	mat4 trackballRotation; // trackball rotation matrix
	// ----------------------------------
	const float sunSpeed = 0.035f;
	const float skySpeed = 0.09f;

	Mesh* skybox;
	float rotSky = 0.0f;

	float radius;
	float rotSun = 0.0f;

	// resources
	Mesh *baobabMesh, *planetSmoothMesh, *planetMesh;
	Mesh *planetExplosion, *explosionBanner, *explosionBanner2, *gameOverBanner, * explosionBanner3;
	AnimatedMesh* wolfMesh;
	GLuint treeTexture;

	// todo all game objects to compute collisions
	vector<GameObject*> rigidObjects;

	// simple UI
	ProgressBarObject* planetHealthProgress;
	ProgressBarObject* baobabHealthProgress;

	GameObject* planet;
	GameObject* lamp;
	PrinceObject* prince;
	Object *boxDir, *wolf, *sun;

	GameObject *can, *flower, *bowl, *box, *oldman;

	queue<BaobabObject*> waiting_baobabs;
	unordered_map<GLubyte ,BaobabObject*> baobabs;
	
	GLubyte targetID = 0;
	
	vector<AnimatedBanner*> explosions;

	float health = 100.0f;
	bool gameover;

	void drawSkybox();
	void spawnBaobab();
	void updateBaobabs(float delta);
	void drawBaobabs();
	void createExplosion(const Transform& transform);
	vector<AnimatedBanner*>::iterator destroyExplosion(vector<AnimatedBanner*>::iterator it);
public:

	static const float cosSunShadow;
	static const vec3 planetPosition;
	static Lights lights;

	void init(AssetManager& am) override;

	void setGameOver();

	void update(float delta) override;
	void updateExplosions(float delta);

	void draw() override;

	void mouseMove(int x, int y) override;
	void keyPress(unsigned char keyPress, int mouse_x, int mouse_y) override;
	void mouseClick(int button, int state, int i, int y) override;
	void mouseWheel(int wheel, int direction, int i, int y) override;
	void specialKeyPress(int specKeyPressed, int mouse_x, int mouse_y) override {}
	void passiveMouseMove(int i, int y) override;

	void disposeObjects() override;
};
