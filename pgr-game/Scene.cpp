#include "Scene.h"


void Scene::draw() {
	for (int i = 0, l = objects.size(); i < l; i++) objects[i]->draw(camera);

	sortTransparentObjects();
	for (auto it = transparents.rbegin(); it != transparents.rend(); ++it) it->second->draw(camera);
	for (int i = 0, l = uiObjects.size(); i < l; i++) uiObjects[i]->draw(uiCamera);
}

void Scene::update(float delta) {
	for (int i = 0, l = objects.size(); i < l; i++) objects[i]->update(delta);
	for (int i = 0, l = uiObjects.size(); i < l; i++) uiObjects[i]->update(delta);
	for (auto it = transparents.rbegin(); it != transparents.rend(); ++it) it->second->update(delta);
	camera->update();
	uiCamera->update();
}

void Scene::initObjects() {
	for (int i = 0, l = objects.size(); i < l; i++) objects[i]->init();
	for (int i = 0, l = uiObjects.size(); i < l; i++) uiObjects[i]->init();
	for (auto it = transparents.rbegin(); it != transparents.rend(); ++it) it->second->init();
}

void Scene::reshape(int w, int h) {
	camera->reshape(w, h);
	uiCamera->reshape(w, h);
}

void Scene::sortTransparentObjects() {
	map<float, Object*> tmp;
	for (auto it = transparents.rbegin(); it != transparents.rend(); ++it) tmp[cameraDistance(camera, it->second)] = it->second;
	transparents.swap(tmp);
}

void Scene::reinit(AssetManager& am) {
	done = false;
	if (! ready) {
		init(am);
		initObjects();
		ready = true;
	}
}

Scene::~Scene() {
	disposeObjects();
	delete camera;
	delete uiCamera;
}

void Scene::disposeObjects() {
	for (int i = 0, l = objects.size(); i < l; i++) delete objects[i];
	for (int i = 0, l = uiObjects.size(); i < l; i++) delete uiObjects[i];
	for (auto& it : transparents) delete it.second;
	objects.clear();
	uiObjects.clear();
	transparents.clear();
}
