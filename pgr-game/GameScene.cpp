﻿#include "GameScene.h"
#include "PlanetObject.h"
#include "Input.h"
#include <glm/gtx/vector_angle.inl>
#include <glm/gtx/string_cast.hpp>
#include "BaobabObject.h"
#include "ProgressBarObject.h"
#include <iostream>
#include "BouncingBanner.h"
#include "SceneManager.h"

const float GameScene::cosSunShadow = 0.5f;
const vec3 GameScene::planetPosition = vec3(0.0f);
Lights GameScene::lights = Lights();

void GameScene::init(AssetManager& am) {
	camera->eye.z = 20.0f;
	skybox = am.mesh("skybox");
	// --------- Lights ------------
	// ----- meshes -------
	baobabMesh = am.mesh("tree");
	treeTexture = am.texture("tree2");

	planetMesh = am.mesh("planet");
	planetSmoothMesh = am.mesh("planet_smooth");

	planetExplosion = am.mesh("planet_explosion");
	explosionBanner = am.mesh("explosion");
	explosionBanner2 = am.mesh("explosion2");
	explosionBanner3 = am.mesh("explosion3");

	gameOverBanner = am.mesh("gameover");

	wolfMesh = am.animatedMesh("wolf_walk");

	Mesh *boxMesh = am.mesh("box"),
			*princeMesh = am.mesh("prince"),
			*flowerMesh = am.mesh("flower"),
			*axeMesh = am.mesh("axe"),
			*bowlMesh = am.mesh("bowl"),
			*canMesh = am.mesh("can"),
			*lampMesh = am.mesh("lamp"),
			*oldmanMesh = am.mesh("oldman"),
			*progressMesh = am.mesh("progress");

	//planetMesh->material->useDiffuseMap = true;
	// --------- PLANET OBJECTS--------------
	planet = new GameObject(planetMesh, 0.0f, false);
	planet->transform.scale *= 5.0f;
	planet->useFog = false;
	planet->material = new Material(planet->mesh->material);
	planet->material->specular *= 0.1f;
//	planet->texture = am.texture("moon");
	//	planet->material->useNormalMap = true;
	//	planet->normalMap = am.texture("moon_map2");
	radius = planet->getW() * 5.0f / 2.0f;
	// surrounding planets
	int numPlanets = rand() % 2 + 3;
	for (int i = 0; i < numPlanets; i++) {
		if (rand() % 2 || i == 0)
			objects.push_back(new PlanetObject(planetMesh, am.texture("planet2")));
		else
			objects.push_back(new PlanetObject(planetMesh));
	}

	sun = new Object(planetMesh);
	sun->shader = am.shader("texture");
	sun->texture = am.texture("sun");
	sun->transform.scale *= 80.0f;
	objects.push_back(sun);

	// --------- GAME OBJECTS --------------
	// Prince 
	prince = new PrinceObject(princeMesh, radius + 0.28f);
	prince->transform.scale *= 0.3f;
	prince->moveToPosition(normalize(vec3(0.2f, 1.f, 0.0f)) * prince->radius);
	prince->computePositionAndRotation();

	boxDir = new Object(boxMesh);
	boxDir->transform.scale *= 0.02f;

	// axe 
	GameObject* axe = new GameObject(axeMesh, radius + 0.25f);
	axe->transform.scale *= 0.3f;
	axe->moveToPosition(normalize(vec3(0.1f, 0.1f, 0.0f)) * axe->radius);
	axe->rotateInDeg(20.0f);
	axe->computePositionAndRotation();

	// can
	can = new GameObject(canMesh, radius + 0.16f);
	can->moveToPosition(normalize(vec3(0.01f, 0.02f, -0.04f)) * can->radius);
	can->rotateInDeg(-20.0f);
	can->transform.scale *= 0.3f;

	// flower
	flower = new GameObject(flowerMesh, radius + 0.11f);
	objects.push_back(flower);
	flower->transform.scale *= 0.23f;

	// bowl
	bowl = new GameObject(bowlMesh, radius + 0.23f);
	bowl->transform.scale *= 0.3f;

	//	wolf = new Object(wolfMesh);
	//	wolf->transform.position.y = radius + 0.5f;

	box = new GameObject(boxMesh, radius + 0.11f);
	box->transform.scale *= 0.2f;
	box->moveToPosition(normalize(vec3(-1.0f, -0.8f, -0.8f)) * box->radius);
	box->rotateInDeg(60.0f);
	box->computePositionAndRotation();

	// lamp and point light
	lamp = new GameObject(lampMesh, radius + 0.7f - 0.07f);
	lamp->transform.scale *= 0.7f;
	lamp->moveToPosition(normalize(vec3(-1.0f, -1.0f, -1.0f)) * lamp->radius);
	lamp->computePositionAndRotation();
	lights.lamp.position = normalize(lamp->transform.position) * (lamp->radius + 0.4f);

	oldman = new GameObject(oldmanMesh, radius + 0.28f);
	oldman->transform.scale *= 0.35f;
	oldman->moveToPosition(normalize(vec3(-1.0f, -1.1f, -0.9f)) * oldman->radius);
	oldman->rotateInDeg(-20);
	oldman->computePositionAndRotation();

	rigidObjects.push_back(prince);
	rigidObjects.push_back(lamp);
	rigidObjects.push_back(box);
	rigidObjects.push_back(bowl);
	rigidObjects.push_back(can);

	objects.push_back(planet);
	objects.push_back(can);
	objects.push_back(lamp);
	objects.push_back(box);
	objects.push_back(axe);
	objects.push_back(boxDir);
	objects.push_back(prince);
	objects.push_back(oldman);
	//	objects.push_back(wolf);

	addTransparentObject(bowl);

	for (GLubyte i = 0; i < BaobabObject::maxCount; i++) {
		BaobabObject* b = new BaobabObject(baobabMesh, radius);
		waiting_baobabs.push(b);
		rigidObjects.push_back(b);
	}
	// --------- UI OBJECTS --------------

	baobabHealthProgress = new ProgressBarObject(progressMesh);
	baobabHealthProgress->alignTop();
	planetHealthProgress = new ProgressBarObject(progressMesh);

	uiObjects.push_back(baobabHealthProgress);
	uiObjects.push_back(planetHealthProgress);
	// --------- --------- --------- -----
	lastEye = camera->eye;
	lastCenter = camera->center;
	lastUp = camera->up;
	cameraDistance = distance(camera->eye, camera->center);
	Object::USE_FOG_GLOBAL = false;
	glutSetCursor(GLUT_CURSOR_CROSSHAIR);
}


void GameScene::update(float delta) {
	rotSky += skySpeed * delta;

	if (Input::ESC_PRESSED) {
		setDone();
		Input::ESC_PRESSED = false;
	}

	if (gameover) {
		Scene::update(delta);
		return;
	}

	mat4 rmat = glm::rotate(mat4(1.0f), sunSpeed * delta, vec3(0.0f, 0.0f, 1.0f));
	lights.sun.direction = vec3(rmat * vec4(lights.sun.direction, 0.0f));
	lights.sun.position = vec3(rmat * vec4(lights.sun.position, 0.0f));
	sun->transform.position = lights.sun.position;
	lights.flash.position = prince->transform.position;

	if (health < 0) setGameOver();

	if (Input::T_PRESSED) {
		Input::T_PRESSED = false;
		if (lights.flash.spotCosCutOff < 1) lights.flash.spotCosCutOff = 1;
		else lights.flash.spotCosCutOff = 0.93f;
	}

	if (Input::SPACE_PRESSED) {
		Input::SPACE_PRESSED = false;
		Object::USE_FOG_GLOBAL = prince->POV = POV = !POV;
		if (POV) {
			glutSetCursor(GLUT_CURSOR_NONE);
			lastEye = camera->eye;
			lastCenter = camera->center;
			lastUp = camera->up;
			prince->updateCamera(camera, cameraDistance);
			planet->mesh = planetSmoothMesh;
		} else {
			glutSetCursor(GLUT_CURSOR_CROSSHAIR);
			camera->setTransition(lastCenter, lastEye, lastUp, 1.0f);
			planet->mesh = planetMesh;
		}
	}

	if (camera->inTransition()) camera->updateTransitionTime(delta);

	boxDir->transform.position = prince->transform.position + (prince->direction * 0.35f);
	boxDir->transform.rotation = prince->transform.rotation;
	boxDir->transform.localRotation = prince->transform.localRotation;

	if (BaobabObject::shouldSpawn(delta) && waiting_baobabs.size() > 0) spawnBaobab();

	updateBaobabs(delta);
	updateExplosions(delta);

	if (POV) {
		prince->updateCamera(camera, cameraDistance);
		lights.flash.direction = normalize(camera->center - camera->eye);

		glReadPixels(glutGet(GLUT_WINDOW_WIDTH) / 2, glutGet(GLUT_WINDOW_HEIGHT) / 2, 1, 1, GL_STENCIL_INDEX, GL_UNSIGNED_BYTE, &targetID);

		if (Input::MOUSE_LEFT_PRESSED && targetID > 0) {
			Input::MOUSE_LEFT_PRESSED = false;
			BaobabObject* b = baobabs[targetID];
			if (b != NULL) {
				b->recieveDamage(prince->damage);
				if (b->health <= 0) {
					waiting_baobabs.push(b);
					baobabs.erase(b->id);
					b->isRigid = false;
					createExplosion(b->transform);
				}
			}
		}
	} else
		lights.flash.direction = normalize(prince->direction);

	if (targetID > 0)
		baobabHealthProgress->setProgress(baobabs.find(targetID) != baobabs.end() ? baobabs[targetID]->health : 0);
	else
		baobabHealthProgress->setProgress(0);

	planetHealthProgress->setProgress(health);
	prince->rigidObjects = rigidObjects;
	wolfMesh->currentTime += delta;
	Scene::update(delta);
}

void GameScene::spawnBaobab() {
	BaobabObject* b = waiting_baobabs.front();
	waiting_baobabs.pop();
	b->respawn(rigidObjects, rand() % 2 ? treeTexture : b->mesh->texture, radius);
	baobabs[b->id] = b;
}

void GameScene::updateBaobabs(float delta) {
	for (auto& it : baobabs) {
		it.second->update(delta);
		if (it.second->id == targetID)
			it.second->material->ambient.r = 10.0f;
		else
			it.second->material->ambient.r = it.second->mesh->material->ambient.r;

		it.second->computeCollision(prince);
		health -= it.second->damageToPlanet();
	}
}

void GameScene::createExplosion(const Transform& transform) {
	AnimatedBanner* explosion;
	int r = rand() % 3;
	if (r == 0)
		explosion = new AnimatedBanner(explosionBanner, AnimatedBanner::L2R_T2B, 9, 9, 2.0f, false);
	else if (r == 1){
		explosion = new AnimatedBanner(explosionBanner2, AnimatedBanner::L2R_T2B, 8, 6, 1.5f, false);
		explosion->transform.scale.y *= 8.0f / 6.0f;
	} else 
		explosion = new AnimatedBanner(explosionBanner3, AnimatedBanner::L2R_T2B, 8, 8, 3.0f, false);
	
	explosion->transform.position = transform.position;
	explosion->transform.scale *= glm::min(1.0f, transform.scale.x);
	addTransparentObject(explosion);
	explosions.push_back(explosion);
}

void GameScene::updateExplosions(float delta) {
	auto it = explosions.begin();
	while (it != explosions.end()) {
		if ((*it)->played) it = destroyExplosion(it);
		else ++it;
	}
}

vector<AnimatedBanner*>::iterator GameScene::destroyExplosion(vector<AnimatedBanner*>::iterator it) {
	auto it2 = transparents.begin();
	while (it2 != transparents.end() && it2->second != *it) { ++it2; }
	if (it2 != transparents.end()) transparents.erase(it2);
	delete *it;
	return explosions.erase(it);
}


void GameScene::keyPress(unsigned char keyPress, int mouse_x, int mouse_y) {
	if (!POV) {
		switch (keyPress) {
			case 'r':
				camera->setTransition(vec3(0.0f, 0.0f, 0.0f), vec3(0.0f, 0.0f, 20.0f), vec3(0.0f, 1.0f, 0.0f), 1.0f);
				break;
			case '1':
				camera->setTransition(camera->center, front_eye, front_up, 1.0f);
				break;
			case '2':
				camera->setTransition(camera->center, back_eye, back_up, 1.0f);
				break;
			case '3':
				camera->setTransition(camera->center, left_eye, left_up, 1.0f);
				break;
			case '4':
				camera->setTransition(camera->center, right_eye, right_up, 1.0f);
				break;
			case '5':
				camera->setTransition(camera->center, top_eye, top_up, 1.0f);
				break;
			case '6':
				camera->setTransition(camera->center, bottom_eye, bottom_up, 1.0f);
				break;
			case 'g':
				health -= 101;
				break;
		}
	}
}

void GameScene::mouseMove(int x, int y) {
	endGrabX = x;
	endGrabY = y;
	if (!POV) {
		if (Input::MOUSE_LEFT_PRESSED) {
			if (startGrabX != endGrabX || startGrabY != endGrabY) {
				trackballRotation = glm::rotate(mat4(1.0f), (endGrabY - startGrabY) * 0.1f, cross(camera->eye, camera->up));
				trackballRotation = glm::rotate(trackballRotation, (endGrabX - startGrabX) * 0.1f, camera->up);
				camera->eye = vec3(trackballRotation * vec4(camera->eye, 1.0f));
				camera->up = vec3(trackballRotation * vec4(camera->up, 0.0f));
			}
		}
	}
	startGrabX = endGrabX;
	startGrabY = endGrabY;
}

void GameScene::mouseClick(int button, int state, int x, int y) {
	if (button == GLUT_LEFT_BUTTON) {
		startGrabX = x;
		startGrabY = y;
	} else if (button == GLUT_RIGHT_BUTTON) {
		glReadPixels(x, glutGet(GLUT_WINDOW_HEIGHT) - 1 - y, 1, 1, GL_STENCIL_INDEX, GL_UNSIGNED_BYTE, &targetID);
	}
}

void GameScene::mouseWheel(int wheel, int direction, int x, int y) {
	if (!POV) {
		if (direction == 1)
			camera->eye *= 0.9f;
		else if (direction == -1)
			camera->eye *= 1.1f;
	}
}

void GameScene::passiveMouseMove(int x, int y) {
	endGrabX = x;
	endGrabY = y;
	if (POV) {
		if (startGrabX != endGrabX) { prince->turn((startGrabX - endGrabX) * 0.005f); }
		if (startGrabY != endGrabY) { prince->lookUp((startGrabY - endGrabY) * 0.18f); }
		if (x >= glutGet(GLUT_WINDOW_WIDTH) - 15 || x <= 7)
			glutWarpPointer(x = endGrabX = glutGet(GLUT_WINDOW_WIDTH) / 2, y);
		if (y >= glutGet(GLUT_WINDOW_HEIGHT) - 15 || y <= 7)
			glutWarpPointer(x, endGrabY = glutGet(GLUT_WINDOW_HEIGHT) / 2);
	}
	startGrabX = endGrabX;
	startGrabY = endGrabY;
}

void GameScene::drawSkybox() {
	mat4 R = glm::rotate(mat4(1.0f), rotSky, vec3(0.5f, 1.0f, 0.0f));
	mat4 viewRotation(camera->view);
	viewRotation[3] = vec4(0.0f, 0.0f, 0.0f, 1.0f);
	mat4 invPV = inverse(camera->projection * viewRotation * R);
	glUseProgram(skybox->shader->program);
	glBindVertexArray(skybox->vao);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_CUBE_MAP, skybox->texture);
	GLint invPVLoc = skybox->shader->get("invPV");
	glUniformMatrix4fv(invPVLoc, 1, GL_FALSE, value_ptr(invPV));
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
	glUseProgram(0);
	glBindVertexArray(0);
}

void GameScene::draw() {
	drawSkybox();
	drawBaobabs();
	Scene::draw();
}

void GameScene::drawBaobabs() {
	glEnable(GL_STENCIL_TEST);
	glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);
	for (auto& b : baobabs) {
		glStencilFunc(GL_ALWAYS, b.second->id, 255);
		b.second->draw(camera);
	}
	glDisable(GL_STENCIL_TEST);
}

void GameScene::disposeObjects() {
	rigidObjects.clear();
	for (unsigned i = 0; i < baobabs.size(); i++)
		delete baobabs[i];
	baobabs.clear();
	while (!waiting_baobabs.empty()) {
		BaobabObject* b = waiting_baobabs.front();
		waiting_baobabs.pop();
		delete b;
	}
	baobabs.clear();
	Scene::disposeObjects();
}

void GameScene::setGameOver() {
	POV = false;
	gameover = true;
	camera->setTransition(vec3(0.0f, 0.0f, 0.0f), vec3(0.0f, 0.0f, 20.0f), vec3(0.0f, 1.0f, 0.0f), 1.0f);
	float sc = planet->getScX();
	disposeObjects();
	objects.push_back(new AnimatedBanner(planetExplosion, AnimatedBanner::L2R_T2B, 8, 4, 2.0f));
	objects[0]->transform.scale.x *= sc * 0.8f;
	objects[0]->transform.scale.y *= sc * 0.8f * 2;
	uiObjects.push_back(new BouncingBanner(gameOverBanner));
	uiObjects[0]->transform.scale *= SceneManager::WINDOW_WIDTH / SceneManager::WINDOW_HEIGHT;
}
