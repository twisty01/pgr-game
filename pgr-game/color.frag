#version 140

out vec4 outputColor;

uniform vec4 color;

void main() {
	outputColor = color;
}