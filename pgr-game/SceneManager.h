/**
* Adam Fi�er 16.05.2017
*/
#pragma once
#include "Scene.h"
#include "AssetManager.h"
#include "GameScene.h"
#include "LoadingScene.h"

/**
 * This class manages scene callbacks, draw, updates and their switching.
 */
class SceneManager {

	Scene* currentScene;
	LoadingScene* loadingScene;
	GameScene* gameScene;

public:
	// todo inputManager???
//	static int LAST_X, LAST_Y;
//	static bool MOUSE_LEFT_PRESSED;
//	static bool MOUSE_RIGHT_PRESSED;
	static int WINDOW_WIDTH;
	static int WINDOW_HEIGHT;
	static bool USE_MATERIALS;

	bool reload = false;

	AssetManager am;

	SceneManager() {}
	~SceneManager();

	/**
	 * \brief Init scene objects.
	 */
	void init();
	/**
	 * \brief Sets loading progress to loading scene.
	 */
	void setProgress(float progress);
	void finishLoading();
	void switchScenes();
	/**
	* \brief Current scene update.
	*/
	void update(float elapsed);
	/**
	* \brief Current scene draw.
	*/
	void draw();
	/**
	* \brief Current scene input callbacks.
	*/
	void keyPress(unsigned char key_pressed, int mouse_x, int mouse_y);
	void keyUp(unsigned char key_pressed, int mouse_x, int mouse_y);
	void specialKeyPress(int specKeyPressed, int mouse_x, int mouse_y);
	void specialKeyUp(int specKeyPressed, int mouse_x, int mouse_y);
	void reshape(int w, int h);
	void mouseMove(int x, int y);
	void mouseClick(int button, int state, int i, int y);
	void mouseWheel(int wheel, int direction, int i, int y);
	void passiveMouseMove(int i, int y);

};
