﻿#include "AnimatedBanner.h"
#include "Camera.h"

void AnimatedBanner::updateTime(float time) {
	currentTime += time;
	if (currentTime > animationTime) {
		if ( loop )
			currentTime -= animationTime;
		else 
			currentTime = animationTime - 0.0001f;

		played = true;
	}
	currentFrame = int(frames * currentTime / animationTime);
	switch (order) {
		case L2R_T2B:
			currentCol = currentFrame % numCols;
			currentRow = numRows - currentFrame / numCols - 1;
			break;
		case R2L_B2T:
			currentCol = numCols - currentFrame % numCols - 1;
			currentRow = currentFrame / numCols;
			break;
		case R2L_T2B:
			currentCol = numCols - currentFrame % numCols - 1;
			currentRow = numRows - currentFrame / numCols - 1;
			break;
		case L2R_B2T:
		default:
			currentCol = currentFrame % numCols;
			currentRow = currentFrame / numCols;
	}
}

void AnimatedBanner::updateAttribs() {
	Object::updateAttribs();
	GLint numColsL = shader->get("numCols");
	GLint numRowsL = shader->get("numRows");
	GLint curColL = shader->get("currentCol");
	GLint curRowL = shader->get("currentRow");
	GLint animatedL = shader->get("animated");
	if (animatedL != -1) glUniform1i(animatedL, true);
	if (numColsL != -1) glUniform1i(numColsL, numCols);
	if (numRowsL != -1) glUniform1i(numRowsL, numRows);
	if (curColL != -1) glUniform1i(curColL, currentCol);
	if (curRowL != -1) glUniform1i(curRowL, currentRow);
}

void AnimatedBanner::clearAttribs() {
	glUseProgram(shader->program);
	GLint animatedL = shader->get("animated");
	if (animatedL != -1) glUniform1i(animatedL, false);
	glUseProgram(0);
}

void AnimatedBanner::update(float delta) { updateTime(delta); }

void AnimatedBanner::draw(Camera* camera) {
	faceCamera(camera);
	Object::draw(camera);
	clearAttribs();
}

void AnimatedBanner::faceCamera(Camera* camera) {
	transform.rotation = transpose(mat4(
		camera->view[0],
		camera->view[1],
		camera->view[2],
		vec4(0, 0, 0, 1)
	));
}
