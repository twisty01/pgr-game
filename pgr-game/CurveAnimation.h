﻿/**
* Adam Fišer 16.05.2017
*/
#pragma once
#include <pgr.h>

class Transform;
using namespace glm;

class CurveAnimation {
public:

	static bool isVectorNull(const vec3& vect) { return !vect.x && !vect.y && !vect.z; }

	static mat4 alignObject(const vec3& position, const vec3& front, const vec3& up);

	static void alignObject(const vec3& position, const vec3& direction, const vec3& baseDirection, Transform &transform);

	static vec3 evaluateCurveSegment(const vec3& P0, const  vec3& P1, const  vec3& P2, const  vec3& P3, const float t);

	static vec3 evaluateCurveSegment_1stDerivative(const vec3& P0, const  vec3& P1, const  vec3& P2, const  vec3& P3, const float t);

	static vec3 evaluateClosedCurve(const vec3 points[], const size_t count, const float t);
	
	static vec3 evaluateClosedCurve_1stDerivative(const vec3 points[], const size_t count, const float t);

};
