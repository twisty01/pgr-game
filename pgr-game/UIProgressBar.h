#pragma once
#include "Object.h"

class UIProgressBar : public Object
{
public:
	glm::vec3 color = glm::vec3(1.0f,0.0f,0.0f);
	float progress = 1.0f;

	explicit UIProgressBar(Mesh* mesh)
		: Object(mesh)
	{
	}
};

