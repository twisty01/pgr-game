﻿#include "ProgressBarObject.h"
#include "SceneManager.h"

ProgressBarObject::ProgressBarObject(Mesh* mesh, GLuint texture): Object(mesh, texture) { initProgressBar(); }
ProgressBarObject::ProgressBarObject(Mesh* mesh): Object(mesh) { initProgressBar(); }

void ProgressBarObject::initProgressBar() {
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture);
	float color[4] = {0.0f,0.0f,0.0f,0.0f};
	glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, color);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);
	glBindTexture(GL_TEXTURE_2D, 0);

	trX(-float(SceneManager::WINDOW_WIDTH) / SceneManager::WINDOW_HEIGHT);
	transform.position.y = -1.0f;
	transform.scale.y = 0.01f;
	transform.scale.x = 0.0f;
}

void ProgressBarObject::alignTop() { transform.position.y = 1.0f - getH() * getScY() / 2.0f - 0.031f; }

void ProgressBarObject::setProgress(float p) {
	progress = p;
	transform.scale.x = progress / maxProgress;
}
