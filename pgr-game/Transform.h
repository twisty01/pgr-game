/**
* Adam Fi�er 16.05.2017
*/
#pragma once
#include "pgr.h"


using namespace glm;

class Transform {
	mat4 model;
public:

	vec3 position;
	vec3 scale;
	
	//! computed 1. -> axis is it is
	mat4 rotation;

	//! computed 2. -> with rotated axis by 1. rotation
	mat4 localRotation = mat4(1.0f);

	explicit Transform(const vec3& position = vec3(0.0f), const vec3& scale = vec3(1.0f))
		: model(1.0f),  position(position), scale(scale), rotation(1.0f) {}

	mat4 computeTransform(mat4 modelMatrix = mat4(1.0f)) {
		mat4 scaleMatrix = glm::scale(modelMatrix, scale);
		mat4 translateMatrix = translate(modelMatrix, position);
		return model = translateMatrix  * localRotation * rotation  * scaleMatrix;
	}

	mat4 getModelMatrix() const { return model; }

};
