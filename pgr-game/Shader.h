/**
* Adam Fi�er 16.05.2017
*/
#pragma once
#include <gl_core_4_4.h>
#include <unordered_map>

using namespace std;

// basic shader
class Shader {
public:
	static const char* VERTEX_SHADER_EXTENSION;
	static const char* FRAGMENT_SHADER_EXTENSION;
	// identifier for the program
	GLuint program = 0;
	// vertex attributes locations
	unordered_map<string, GLint> locations;

	Shader(const string& vertex, const string& fragment);
	Shader(const string& vertexSrc, const string& fragmentSrc, bool);
	Shader() {}
	~Shader() { glDeleteProgram(program); }

	Shader * initAttr(const string& attribName);
	Shader * initUni(const string& uniformName);
	GLint get(const string &name);
	GLint getDefUni(const char* name);
}; 

