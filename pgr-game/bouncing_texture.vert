#version 140

uniform float time;

uniform mat4 PVM;

in vec2 uv;
in vec3 position;
out vec2 _uv;

void main() {
	gl_Position = PVM * vec4(position, 1.0);
	
	float localTime = time * 0.1;
	vec2 offset = vec2((localTime - floor(localTime)) - 0.5, 0.0);
	_uv = uv + offset;
}