/**
* Adam Fi�er 16.05.2017
*/
#pragma once
#include <pgr.h>

using namespace glm;

class Camera {

	bool transition = false;
	vec3 targetCenter;
	vec3 targetEye;
	vec3 targetUp;


public:

	float currentTime;
	float animTime;
	
	static const char PERSPECTIVE = 1, ORTHO = 0;

	char type;

	vec3 eye, center, up;
	mat4 view, projection;


	Camera(vec3 eye, vec3 center, vec3 up, mat4& projection_matrix, char type)
		: eye(eye),
		  center(center),
		  up(up),
		  view(lookAt(eye, center, up)),
		  projection(projection_matrix), type(type) {}

	Camera(vec3 eye, vec3 center, vec3 up, int width, int height, const char type);

	Camera(int width, int height, const char type)
		: Camera(vec3(0.0f, 0.0f, 20.0f),
		         vec3(0.0f, 0.0f, 0.0f),
		         vec3(0.0f, 1.0f, 0.0f), width, height, type) {}

	Camera(const char type) : Camera(glutGet(GLUT_WINDOW_WIDTH), glutGet(GLUT_WINDOW_HEIGHT), type) {}
	Camera() {}

	void reshape(int w, int h);

	void update();


	void updateTransitionTime(float delta) { currentTime += delta; }

	bool inTransition() const { return transition; }

	void setTransition(vec3 targetCenter, vec3 targetEye, vec3 targetUp, float animTime);

	void finishTransition();

};
