﻿#pragma once
#include "Object.h"

class Baobab : public Object
{
public:
	const float TIME = 15.0f;
	float time;
	glm::vec3 position; // in angles


	explicit Baobab(Mesh* mesh)
		: Object(mesh)
	{
	}

};
