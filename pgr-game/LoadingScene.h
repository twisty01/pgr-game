﻿/**
* Adam Fišer 16.05.2017
*/
#pragma once
#include "Scene.h"
#include "ProgressBarObject.h"

class LoadingScene : public Scene {

	Object* splashBanner;
	GLint loadedTexture;
	GLuint controlsTexture;

	ProgressBarObject* progressBanner;

	Object* light;
	Object* comet;
	
	Object* test;

	bool loaded = false;
	bool reload = false;
	bool controls = false;

public:

	void init(AssetManager& am) override;

	void update(float delta) override;

	void setProgress(float progress);

	void finishLoading();

	bool shouldReload();

};
