﻿/**
* Adam Fišer 16.05.2017
*/
#pragma once
#include "Object.h"

class ProgressBarObject : public Object {
public:
	float maxProgress = 100.0f;
	float progress = 0.0f;

	ProgressBarObject(Mesh* mesh, GLuint texture);
	ProgressBarObject(Mesh* mesh);

	void initProgressBar();

	void alignTop();

	void setProgress(float p);

};
