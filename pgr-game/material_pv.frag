#version 140

 // structure that describes currently used material
struct Material {          
  vec3  ambient;
  vec3  diffuse;
  vec3  specular;
  float shininess;
  bool  useTexture;
};

uniform sampler2D texSampler;

uniform Material material;

in vec4  _color; // incoming fragment color (includes lighting)
in vec2  _uv;
out vec4 outputColor;

void main() {
  // if material has a texture -> apply it
  if(material.useTexture)
	outputColor =  _color * texture(texSampler, _uv );
  else
	outputColor = _color;
}
