#include "GameObject.h"
#include "glm/ext.hpp"
#include "GameScene.h"

using namespace glm;

void GameObject::updateAttribs() { GameScene::lights.setLightUniforms(shader, GameScene::cosSunShadow, computeSunShadow, GameScene::planetPosition); }

void GameObject::update(float delta) { computePositionAndRotation(); }

void GameObject::computePositionAndRotation() {
	if (radius == 0) return;
	// basePos -> actualPos -> angle axis  -> rotate baseDirAxis
	float a = angle(normalize(basePos), normalize(transform.position));
	if (a != 0) {
		vec3 rotationAxis = cross(basePos, transform.position);
		transform.rotation = rotate(mat4(1.0f), a, rotationAxis);
	}

	// rotate base dirAxis and baseDir -> angle between rotated dir and actual dir
	vec3 dirAxis = vec3(transform.rotation * vec4(basePos, 0.0)); // basePos ~ baseDirAxis
	vec3 dir = vec3(transform.rotation * vec4(baseDir, 0.0));

	float dirAngle = orientedAngle(normalize(dir), normalize(direction), dirAxis);
	if (dirAngle != 0)
		transform.localRotation = rotate(mat4(1.0f), dirAngle, dirAxis);
}

void GameObject::move(float d) { moveToPosition(normalize(transform.position + (direction * speed * d)) * radius); }

void GameObject::turn(float d) { rotateInDeg(d * lookSpeed); }

// --------------------------------------------------------------

void GameObject::moveToPosition(vec3 newPosition) {
	vec3 oldPos = transform.position;
	transform.position = newPosition;
	vec3 axis = cross(oldPos, direction);
	float a = orientedAngle(normalize(oldPos), normalize(transform.position), axis);
	if (a != 0) {
		mat4 rotation = rotate(mat4(1.0f), a, axis);
		direction = vec3(rotation * vec4(direction, 0.0f));
	}
}

bool GameObject::moveToPosition(vec3 newPosition, vector<GameObject*>& rigidObjects) {
	vec3 oldPos = transform.position;
	transform.position = newPosition;
	for (auto& it : rigidObjects)
		if (it->isRigid && it != this && intersects(it)) {
			transform.position = oldPos;
			return false;
		}
	transform.position = oldPos;
	moveToPosition(newPosition);
	return true;
}

void GameObject::rotateInDeg(float degrees) {
	mat4 rotation = rotate(mat4(1.0f), degrees, transform.position);
	direction = vec3(rotation * vec4(direction, 0.0f));
}

// --------------------------------------------------------------

float GameObject::distance(const Object* object) const { return glm::distance(transform.position, object->transform.position); }

float GameObject::distance(vec3 point) const { return glm::distance(transform.position, point); }

bool GameObject::intersects(const GameObject* o) const {
	float r1 = computeBoundRadius();
	float r2 = o->computeBoundRadius();
	return glm::distance(transform.position, o->transform.position) < (r1 + r2);
}

float GameObject::computeBoundRadius() const { return glm::max(getW() * getScX(), getD() * getScZ()); }
