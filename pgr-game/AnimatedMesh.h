﻿/**
* Adam Fišer 16.05.2017
*/

#pragma once
#include "Mesh.h"

class AnimatedMesh : public Mesh {

public:
	static const char NORMAL = 0, REVERSE = 1, LOOP = 2, REVERSE_LOOP = 3, MIRROR_LOOP = 4;

	int numFrames, currentFrame;
	float animationTime, currentTime;
	char type;

	AnimatedMesh(const float* vertices, int numVertices,
	             const unsigned* indices, int numIndices,
				 bool has_normals, bool has_uvs,
	             Shader* shader,
	             Material* material,
	             GLuint texture,
	             int num_frames, float animation_time, char type);

	void updatePositionAndNormals();

	void draw() override;
};
