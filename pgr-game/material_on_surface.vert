#version 140

struct Material {      // structure that describes currently used material
  vec3  ambient;       // ambient component
  vec3  diffuse;       // diffuse component
  vec3  specular;      // specular component
  float shininess;     // sharpness of specular reflection
  bool  useTexture;    // defines whether the texture is used or not
};

// warning: sampler inside the Material struct can cause problems -> so its outside
uniform sampler2D texSampler;  // sampler for the texture access

in vec3 position;           // vertex position in world space
in vec3 normal;             // vertex normal
in vec2 uv;           // incoming texture coordinates

uniform Material material;  // current material

uniform mat4 PVM;     // Projection * View * Model  --> model to clip coordinates
uniform mat4 V;       // View                       --> world to eye coordinates
uniform mat4 M;       // Model                      --> model to world coordinates
uniform mat4 itM;  // inverse transposed Mmatrix

out vec3 _normal;             // vertex normal
out vec2 _uv;  // outgoing texture coordinates
out vec3 _position;

void main() {
  gl_Position = PVM * vec4(position, 1.0);
  _normal = (V * mat4(1.0) * vec4(normal, 0.0) ).xyz;
  _uv = uv;
  _position = (V * M * vec4(position, 1.0)).xyz;   
}
