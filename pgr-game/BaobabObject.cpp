﻿#include "BaobabObject.h"
#include "PlanetObject.h"
#include <iostream>

const float BaobabObject::maxSpawnTime = 15.0f;
const float BaobabObject::minSpawnTime = 5.0f;
const int BaobabObject::maxCount = 25;

int BaobabObject::count = 0;
float BaobabObject::SpawnTimeBound = 5.0f;
float BaobabObject::currentSpawnTime = 0.0f;

bool BaobabObject::shouldSpawn(float delta) {
	currentSpawnTime += delta;
	return currentSpawnTime > SpawnTimeBound;
}

void BaobabObject::resetSpawnTime() {
	currentSpawnTime = 0.0f;
	SpawnTimeBound = minSpawnTime + randf() / (maxSpawnTime - minSpawnTime);
}

BaobabObject::BaobabObject(Mesh* mesh, float planet_radius):
	GameObject(mesh, planet_radius + initialBaobabRadiusOffset){
	isRigid = true;
	transform.scale *= minGrow;
	id = ++count;
	material = new Material(*mesh->material);
}


void BaobabObject::recieveDamage(float dmg) {
	health -= dmg / grow;
}

float BaobabObject::damageToPlanet() const {
	if (grow < maxGrow / 4.0f) return 0.0f;
	return grow * baseDamage;
}

void BaobabObject::draw(Camera* camera) {
	glStencilFunc(GL_ALWAYS, id, 255);
	GameObject::draw(camera);
}

void BaobabObject::update(float delta) {
	grow += delta * 0.008f;
	if (grow < maxGrow / 2.0f)
		radius += delta * 0.004f;
	if (grow > maxGrow) grow = maxGrow;
	transform.scale = vec3(1.0f) * grow;
	basePos = normalize(basePos) * radius;
	transform.position = normalize(transform.position) * radius;
	GameObject::update(delta);
}

void BaobabObject::computeCollision(PrinceObject* prince) const {
	if (intersects(prince)) {
		float a = 0;
		vec3 newPos;
		do {
			mat4 rotM = rotate(mat4(1.0f), a, transform.position);
			float distDelta = abs(prince->computeBoundRadius() - computeBoundRadius());
			vec3 newDir = normalize(vec3(rotM * vec4(prince->transform.position - transform.position, 0.0f))) * distDelta;
			newPos = normalize(prince->transform.position + newDir) * prince->radius;
			a += 20;
		} while (!prince->moveToPosition(newPos, prince->rigidObjects) && a < 360.0f);
	}
}

void BaobabObject::respawn(vector<GameObject*>& rigidObjects, GLuint luint, float planet_radius) {
	radius = initialBaobabRadiusOffset + planet_radius;
	cout << "SPAWN... radius = " << radius <<  endl;
	basePos = normalize(basePos) * radius;
	health = 100;
	grow = minGrow;
	isRigid = true;
	rotateInDeg(randf() * 360.0f);
	vec3 position;
	do {
		vec3 pos = vec3(randf() * 2 - 1, randf() * 2 - 1, randf() * 2 - 1);
		position = normalize(pos) * radius;
	} while (!moveToPosition(position, rigidObjects));
	resetSpawnTime();
}
