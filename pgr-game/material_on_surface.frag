#version 140

 // structure that describes currently used material
struct Material {          
  vec3  ambient;
  vec3  diffuse;
  vec3  specular;
  float shininess;
  bool  useTexture;
};

struct Light {         // structure describing light parameters
  vec3  ambient;       // intensity & color of the ambient component
  vec3  diffuse;       // intensity & color of the diffuse component
  vec3  specular;      // intensity & color of the specular component
  vec3  position;      // light position
  vec3  spotDirection; // spotlight direction
  float spotCosCutOff; // cosine of the spotlight's half angle
  float spotExponent;  // distribution of the light energy within the reflector's cone (center->cone's edge)
};

uniform sampler2D texSampler;

uniform Material material;  // current material

uniform mat4 PVM;     // Projection * View * Model
uniform mat4 V;       // View                       
uniform mat4 M;       // Model                      
uniform mat4 itM;  // inverse transposed Model

in vec2  _uv;
in vec3 _normal;            
in vec3 _position;

out vec4 outputColor;

vec4 directionalLight(Light light, Material material, vec3 vertexPosition, vec3 vertexNormal) {
  vec3 ret = vec3(0.0f);

  vec3 L = normalize(light.position);
  vec3 R = reflect(-L, vertexNormal);
  vec3 C = normalize(-vertexPosition);
  float NdotL = max(0.0f, dot(vertexNormal, L));
  float RdotV = max(0.0f, dot(R, C));

  ret += material.ambient * light.ambient;
  ret += material.diffuse * light.diffuse * NdotL;
  ret += material.specular * light.specular * pow(RdotV, material.shininess);

  return vec4(ret, 0.0f);
}

// hardcoded lights
Light sun;

void setupLights() {
  sun.ambient  = vec3(0.3);
  sun.diffuse  = vec3(1.0);
  sun.specular = vec3(1.0);
  sun.position = (V * vec4(1000.0, 1000.0, 0.0, 0.0)).xyz;
}

void main() {
   setupLights();

  vec3 globalAmbientLight = vec3(0.1f);
  vec4 clr = vec4(material.ambient * globalAmbientLight, 1.0f);
  clr += directionalLight(sun, material, _position , normalize(_normal));

  if(material.useTexture)
	outputColor =  clr * texture(texSampler, _uv );
  else
	outputColor =  texture(texSampler, _uv );
}
