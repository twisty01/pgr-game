﻿/**
* Adam Fišer 16.05.2017
*/
#pragma once
#include "GameObject.h"

class PrinceObject : public GameObject {
public:
	const float view_max_up_angle = 45;
	const float view_min_up_angle = -20;

	const float damage = 12.5f;
	GameObject *axe, *waterCan, *bowl;

	bool POV = false;
	float cameraUpAngle = 0.0f;

	vector<GameObject*> rigidObjects;

	PrinceObject(Mesh* mesh, float planet_radius);
	void lookUp(float angle);
	void lookDown(float angle);

	void update(float delta) override;

	void draw(Camera* camera) override;
	void moveRigid(float d);
	void moveRightRigid(float delta);
	void moveLeftRigid(float delta);

	void updateCamera(Camera* camera,float cameraDistance) const;

	void setAxePosition() { }

	void setAxeAnim() { }


};
