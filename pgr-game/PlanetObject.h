﻿/**
* Adam Fišer 16.05.2017
*/
#pragma once
#include "Object.h"

inline float randf();

class PlanetObject : public Object {


public:
	static float SUN_X;
	static float RADIUS;

	float speed;
	vec3 speedCenter;

	PlanetObject(Mesh* mesh, GLuint texture);
	PlanetObject(Mesh* mesh) : PlanetObject(mesh, mesh->texture){}

	void update(float delta) override;

	void updateAttribs() override;

};

