/**
* Adam Fi�er 16.05.2017
*/
#pragma once

#include "Object.h"
#include "Camera.h"
#include <vector>
#include <map>
#include "AssetManager.h"
#include "pgr.h"

/**
 * \brief Defines Interface for SceneManager to draw objects in a Scene and reacts to input callbacks.
 */
class Scene {
	bool done = true, ready = false;

protected:
	vector<Object *> objects, uiObjects;
	map<float, Object*> transparents;
	Camera *camera, *uiCamera;

public:
	Scene(int width, int height): camera(new Camera(width, height, Camera::PERSPECTIVE)), uiCamera(new Camera(width, height, Camera::ORTHO)) {}
	Scene(): camera(new Camera(Camera::PERSPECTIVE)), uiCamera(new Camera(Camera::ORTHO)) {}

	virtual ~Scene();
	virtual void disposeObjects();

	/**
	* \brief Draws all current objects. First are objects with camera, then transparent objects 
	* sorted by camera distance and the last are uiOjbects with uiCamera.
	*/
	virtual void draw();

	/**
	* \brief Updates implicitly all objects( called update) but contains no logic. Should be overriden for complex logic.
	* \param delta elapsed time from last update in seconds.
	*/
	virtual void update(float delta);

	/**
	* \brief Initializes scene with asset manager to inject assets to scene.
	*/
	virtual void init(AssetManager& am) {}

	void initObjects();
	/**
	 * \brief Called on Resize callback, reshape camera's aspect ratio.
	 */
	void reshape(int w, int h);

	/**
	* \brief Called on Input callbacks
	*/
	virtual void keyPress(unsigned char key_pressed, int mouse_x, int mouse_y) {}
	virtual void keyUp(unsigned char key_pressed, int mouse_x, int mouse_y) {}
	virtual void specialKeyPress(int specKeyPressed, int mouse_x, int mouse_y) {}
	virtual void specialKeyUp(int specKeyPressed, int mouse_x, int mouse_y) {}
	virtual void mouseMove(int mouse_x, int mouse_y) {}
	virtual void mouseClick(int button, int state, int i, int y) {}
	virtual void mouseWheel(int wheel, int direction, int i, int y) {}
	virtual void passiveMouseMove(int i, int y) {}

	static float cameraDistance(Camera* cam, Object* object) { return length(cam->center - object->transform.position); }
	void addTransparentObject(Object* object) { transparents[cameraDistance(camera, object)] = object; }
	void sortTransparentObjects();

	void reinit(AssetManager& am);

	/**
	 * gives scene manager signal, that scene is done and should not be reneder any longer.
	 */
	bool isDone() const { return done; }
	void setDone() { done = true; }
	/**
	* gives scene manager signal, that scene is ready and can be renedere.
	*/
	bool isReady() const { return ready; }
	void setReady() { ready = true; }


};
