#version 140

uniform mat4 PVM;

in vec2 uv;
in vec3 position;
out vec2 _uv;

void main() {
	gl_Position = PVM * vec4(position, 1.0);
    _uv = uv;
}