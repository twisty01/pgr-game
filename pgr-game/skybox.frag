#version 140

uniform samplerCube skybox;

in vec3 _uv;

out vec4 outputColor;

void main() {
    outputColor = texture(skybox, _uv);
}