﻿#include "PrinceObject.h"
#include <glm/gtx/vector_angle.inl>
#include "Input.h"
#include "Camera.h"

PrinceObject::PrinceObject(Mesh* mesh, float planet_radius):
	GameObject(mesh, planet_radius) {
	//
	speed = 2.2f;
}

void PrinceObject::lookUp(float d) {
	cameraUpAngle += d;
	if (cameraUpAngle > view_max_up_angle) cameraUpAngle = view_max_up_angle;
	if (cameraUpAngle < view_min_up_angle) cameraUpAngle = view_min_up_angle;
}

void PrinceObject::lookDown(float d) {
	cameraUpAngle -= d;	
	if (cameraUpAngle > view_max_up_angle) cameraUpAngle = view_max_up_angle;
	if (cameraUpAngle < view_min_up_angle) cameraUpAngle = view_min_up_angle;
}

void PrinceObject::update(float delta) {
	if (Input::W_PRESSED) moveRigid(delta);
	if (Input::S_PRESSED) moveRigid(-delta);
	if (Input::D_PRESSED) {
		if (POV) moveRightRigid(delta);
		else turn(-delta);
	}
	if (Input::A_PRESSED) {
		if (POV) moveLeftRigid(delta);
		else turn(delta);
	}

	GameObject::update(delta);
}

void PrinceObject::updateCamera(Camera* camera, float d) const {
	vec3 up = normalize(transform.position),
			center = normalize(transform.position + (normalize(direction) * d)) * (radius + 2.5f),
			eye = up * (radius + 0.25f),
			camDir = center - eye;
	float a = angle(normalize(camDir), normalize(direction));
	if (a != 0) {
		vec3 rotAxis = cross(camDir, direction);
		mat4 rotMat = rotate(mat4(1.0f), a, rotAxis);
		up = vec3(rotMat * vec4(up, 0.0f));
	}

	// update camera -> rotate by upAngle
	if (cameraUpAngle != 0) {
		vec3 axis = cross(center - eye, up);
		mat4 rotMat = rotate(mat4(1.0f), cameraUpAngle, axis);
		center = vec3(rotMat * vec4(center, 1.0f));
		up = vec3(rotMat * vec4(up, 0.0f));
	}
	camera->setTransition(center, eye, up, camera->inTransition() ? (camera->animTime - camera->currentTime) : 0.0f);
}


void PrinceObject::draw(Camera* camera) {
	if (POV && !camera->inTransition()) return;
	GameObject::draw(camera);
}


void PrinceObject::moveRigid(float d) { moveToPosition(normalize(transform.position + (direction * speed * d)) * radius, rigidObjects); }

void PrinceObject::moveRightRigid(float d) {
	vec3 rightDir = normalize(cross(direction, transform.position));
	moveToPosition(normalize(transform.position + (rightDir * speed * d)) * radius, rigidObjects);
}

void PrinceObject::moveLeftRigid(float d) {
	vec3 leftDir = -normalize(cross(direction, transform.position));
	moveToPosition(normalize(transform.position + (leftDir * speed * d)) * radius, rigidObjects);
}