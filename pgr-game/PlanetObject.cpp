﻿#include "PlanetObject.h"
#include "GameScene.h"


float PlanetObject::RADIUS = 80.0f + randf() * 15.0f;
float PlanetObject::SUN_X = 800.0f;

float randf() { return rand() / float(RAND_MAX); }

PlanetObject::PlanetObject(Mesh* mesh, GLuint texture): Object(mesh, texture) {
	useFog = false;

	speed = (randf() - 0.5f) * 0.0006f;

	speedCenter = vec3(
		(randf() - 0.5f) * 0.005f,
		(randf() - 0.5f) * 0.005f,
		(randf() - 0.5f) * 0.005f);

	transform.scale *= 5.0f + randf() * 10.f + randf() * randf() * randf() * 50.0f;

	float delta = randf() * 50.0f + 55.0f;
	RADIUS += transform.scale.x * 1.1f + delta;
	transform.position.y = transform.scale.x * (randf() *  14.0f - 7.0f);
	transform.position.z = transform.scale.x * (randf() *  14.0f - 7.0f);
	transform.position.x = RADIUS;

	material = new Material();
	material->ambient = vec3(0.15f + randf()/1.4f, 0.15f + randf() / 1.4f, 0.15f + randf() / 1.4f);
	material->diffuse = vec3(1.7f, 1.3f, 1.0f);
	material->specular= vec3(2.0f, 1.5f, 1.3f);
	material->useTexture = true;
}

float r = 0;
void PlanetObject::update(float delta) {
	r += speed * delta;
	transform.rotation = rotate(mat4(1.0f),r,vec3(0,1,0));
}

void PlanetObject::updateAttribs() { GameScene::lights.setLightUniforms(shader, GameScene::cosSunShadow, false, GameScene::planetPosition); }
