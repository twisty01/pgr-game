#include "pgr.h"
#include <ctime>
#include "SceneManager.h"

int WIN_WIDTH = 1600;
int WIN_HEIGHT = 900;
const char* WIN_TITLE = "PGR-Game";

SceneManager manager;

void init() {
	glClearColor(0.0f, 0.0f, 0.06f, 1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthMask(GL_TRUE);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	//glEnable(GL_CULL_FACE);

	glViewport(0, 0, glutGet(GLUT_WINDOW_WIDTH), glutGet(GLUT_WINDOW_HEIGHT));

	glutSetCursor(GLUT_CURSOR_CROSSHAIR);

	SceneManager::WINDOW_WIDTH = WIN_WIDTH;
	SceneManager::WINDOW_HEIGHT = WIN_HEIGHT;
	manager.init();
	CHECK_GL_ERROR();
}

void draw() {
	glStencilMask(~0);
	glDisable(GL_SCISSOR_TEST);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
	manager.draw();
	glutSwapBuffers();
}

void reshape(int w, int h) {
	manager.reshape(w, h);
	glViewport(0, 0, GLsizei(w), GLsizei(h));
}

void mouseMove(int x, int y) { manager.mouseMove(x, y); }
void mouseClick(int button, int state, int x, int y) { manager.mouseClick(button, state, x, y); }
void keyPress(unsigned char k, int mouseX, int mouseY) { manager.keyPress(k, mouseX, mouseY); }
void keyUp(unsigned char k, int mouseX, int mouseY) { manager.keyUp(k, mouseX, mouseY); }
void specialKeyPress(int k, int mouseX, int mouseY) { manager.specialKeyPress(k, mouseX, mouseY); }
void specialKeyUp(int k, int mouseX, int mouseY) { manager.specialKeyUp(k, mouseX, mouseY); }
void mouseWheel(int wheel, int direction, int x, int y) { manager.mouseWheel(wheel, direction, x, y); }
void passiveMouseMove(int x, int y) { manager.passiveMouseMove(x, y); }

int numAssets, currentAsset;

void loadNextAsset() {
	if (manager.am.loadNextAsset()) {
		manager.setProgress(float(++currentAsset) / numAssets * 100.0f);
	} else {
		manager.finishLoading();
		glutIdleFunc(NULL);
	}
}

void loadAssets() {
	numAssets = manager.am.loadAssets("assets.json");
	currentAsset = 0; 
	glutIdleFunc(loadNextAsset);
}


int t1 = 0;

void update(int) {
	int t2 = glutGet(GLUT_ELAPSED_TIME);
	float delta = (t2 - t1) * 0.001f;
	t1 = t2;
	manager.update(delta);
	if (manager.reload) {
		manager.reload = false;
		glutIdleFunc(loadAssets);
	}
	glutTimerFunc(33, update, 0);
	glutPostRedisplay();
}

int main(int argc, char** argv) {
	glutInit(&argc, argv);

	glutInitContextVersion(pgr::OGL_VER_MAJOR, pgr::OGL_VER_MINOR);
	glutInitContextFlags(GLUT_FORWARD_COMPATIBLE);

	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH | GLUT_STENCIL);
	glutInitWindowSize(WIN_WIDTH, WIN_HEIGHT);
	glutCreateWindow(WIN_TITLE);

	glutDisplayFunc(draw);
	glutReshapeFunc(reshape);

	glutKeyboardFunc(keyPress);
	glutKeyboardUpFunc(keyUp);

	glutSpecialFunc(specialKeyPress);
	glutSpecialUpFunc(specialKeyUp);

	glutPassiveMotionFunc(passiveMouseMove);
	glutMotionFunc(mouseMove);
	glutMouseFunc(mouseClick);
	glutMouseWheelFunc(mouseWheel);

	glutTimerFunc(33, update, 0);
	glutIdleFunc(loadAssets);

	if (!pgr::initialize(pgr::OGL_VER_MAJOR, pgr::OGL_VER_MINOR))	pgr::dieWithError("pgr init failed, required OpenGL not supported?");

	init();

	//	glutCloseFunc(dispose);
	glutMainLoop();
	return 0;
}
