﻿/**
* Adam Fišer 16.05.2017
*/
#pragma once
#include <pgr.h>
#include "Shader.h"

class Material {
public:
	glm::vec3 ambient; // ambient component
	glm::vec3 diffuse; // diffuse component
	glm::vec3 specular; // specular component
	float shininess; // sharpness of specular reflection

	bool useTexture = true; // defines whether the texture is used or not
	bool useSpecularMap = false;
	bool useNormalMap = false;

	Material(bool useTexture = true,
	         glm::vec3 ambient = glm::vec3(0.2f, 0.0f, 0.4f),
	         glm::vec3 diffuse = glm::vec3(1.0f, 0.8f, 1.0f),
	         glm::vec3 specular = glm::vec3(1.0f, 1.0f, 1.0f),
	         float shininess = 12.0f,
	         bool useSpecularMap = false,
	         bool useNormalMap = false)
		: ambient(ambient),
		  diffuse(diffuse),
		  specular(specular),
		  shininess(shininess),
		  useTexture(useTexture),
		  useNormalMap(useNormalMap),
		  useSpecularMap(useSpecularMap) {}

	Material(const Material& m) : Material(m.useTexture, m.ambient, m.diffuse, m.specular, m.shininess, m.useSpecularMap, m.useNormalMap) { }

	void setUniforms(Shader* shader) {
		GLint ambiL = shader->get("material.ambient"),
				diffL = shader->get("material.diffuse"),
				specL = shader->get("material.specular"),
				shinL = shader->get("material.shininess"),
				useTexL = shader->get("material.useTexture"),
				useSpecMapL = shader->get("material.useSpecularMap"),
				useNormalMapL = shader->get("material.useNormalMap");

		if (ambiL != -1) glUniform3fv(ambiL, 1, glm::value_ptr(ambient));
		if (diffL != -1) glUniform3fv(diffL, 1, glm::value_ptr(diffuse));
		if (specL != -1) glUniform3fv(specL, 1, glm::value_ptr(specular));
		if (shinL != -1) glUniform1f(shinL, shininess);
		if (useTexL != -1) glUniform1i(useTexL, useTexture);
		if (useSpecMapL != -1) glUniform1i(useSpecMapL, useSpecularMap);
		if (useNormalMapL != -1) glUniform1i(useNormalMapL, useNormalMap);

	}
};
