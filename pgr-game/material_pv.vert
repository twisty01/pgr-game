#version 140

// IMPORTANT: !!! lighting is evaluated in camera space !!!
struct Material {      // structure that describes currently used material
  vec3  ambient;       // ambient component
  vec3  diffuse;       // diffuse component
  vec3  specular;      // specular component
  float shininess;     // sharpness of specular reflection
  bool  useTexture;    // defines whether the texture is used or not
};

// warning: sampler inside the Material struct can cause problems -> so its outside
uniform sampler2D texSampler;  // sampler for the texture access

struct Light {         // structure describing light parameters
  vec3  ambient;       // intensity & color of the ambient component
  vec3  diffuse;       // intensity & color of the diffuse component
  vec3  specular;      // intensity & color of the specular component
  vec3  position;      // light position
  vec3  spotDirection; // spotlight direction
  float spotCosCutOff; // cosine of the spotlight's half angle
  float spotExponent;  // distribution of the light energy within the reflector's cone (center->cone's edge)
};

in vec3 position;           // vertex position in world space
in vec3 normal;             // vertex normal
in vec2 uv;           // incoming texture coordinates

uniform Material material;  // current material

uniform mat4 PVM;     // Projection * View * Model  --> model to clip coordinates
uniform mat4 V;       // View                       --> world to eye coordinates
uniform mat4 M;       // Model                      --> model to world coordinates
uniform mat4 itM;  // inverse transposed Mmatrix

out vec2 _uv;  // outgoing texture coordinates
out vec4 _color;     // outgoing fragment color

vec4 spotLight(Light light, Material material, vec3 vertexPosition, vec3 vertexNormal) {
  vec3 ret = vec3(0.0f);

  vec3 L = normalize(light.position - vertexPosition);
  vec3 R = reflect(-L, vertexNormal);
  vec3 C = normalize(-vertexPosition);

  float NdotL = max(0.0f, dot(vertexNormal, L));
  float RdotV = max(0.0f, dot(R, C));
  float spotCoef = max(0.0f, dot(-L, light.spotDirection));

  ret += material.ambient * light.ambient;
  ret += material.diffuse * light.diffuse * NdotL;
  ret += material.specular * light.specular * pow(RdotV, material.shininess);

  if(spotCoef < light.spotCosCutOff)
    ret *= 0.0f;
  else
    ret *= pow(spotCoef, light.spotExponent);

  return vec4(ret, 0.0f);
}

vec4 directionalLight(Light light, Material material, vec3 vertexPosition, vec3 vertexNormal) {

  vec3 ret = vec3(0.0f);

  vec3 L = normalize(light.position);
  vec3 R = reflect(-L, vertexNormal);
  vec3 C = normalize(-vertexPosition);
  float NdotL = max(0.0f, dot(vertexNormal, L));
  float RdotV = max(0.0f, dot(R, C));

  ret += material.ambient * light.ambient;
  ret += material.diffuse * light.diffuse * NdotL;
  ret += material.specular * light.specular * pow(RdotV, material.shininess);

  return vec4(ret, 0.0f);
}

// hardcoded lights
Light sun;

void setupLights() {
  sun.ambient  = vec3(0.1);
  sun.diffuse  = vec3(1.0, 1.0, 0.5f);
  sun.specular = vec3(1.0);
  sun.position = (V * vec4(4.0, 4.0, 0.0, 1.0)).xyz;

  // set properly reflector direction and position taking into account reflectorPosition
  // and reflectorDirection uniforms
  //reflector.position = (V * vec4(1.0, 1.0, 0.0, 1.0f)).xyz;
  //reflector.spotDirection = normalize((V * vec4(-1.0, -1.0, 0.0, 0.0f)).xyz);
}

void main() {
  gl_Position = PVM * vec4(position, 1);   // out:v vertex in clip coordinates
  
  setupLights();

  vec3 vertexPosition = (V * M * vec4(position, 1.0)).xyz;         // vertex in eye coordinates
  vec3 vertexNormal   = normalize( (V * itM * vec4(normal, 0.0) ).xyz);   // normal in eye coordinates by NormalMatrix
  vec3 globalAmbientLight = vec3(0.1f);

  vec4 clr = vec4(material.ambient * globalAmbientLight, 1.0f);
  clr += directionalLight(sun, material, vertexPosition, vertexNormal);
  //clr += spotLight(reflector, material, vertexPosition, vertexNormal);
  // outputs entering the fragment shader
  _color = clr;
  _uv = uv;
}
