﻿#include "CurveAnimation.h"
#include "Transform.h"
#include <glm/gtx/vector_angle.inl>

/* returns model matrix rotated and translated */
mat4 CurveAnimation::alignObject(const vec3& position, const vec3& front, const vec3& up) {
	vec3 z = -normalize(front);
	if (isVectorNull(z)) z = vec3(0.0f, 0.0f, 1.0f);
	vec3 x = normalize(cross(up, z));
	if (isVectorNull(x)) x = vec3(1.0f, 0.0f, 0.0f);
	vec3 y = cross(z, x);
	mat4 matrix = mat4(
		x.x, x.y, x.z, 0.0f, /* 1. col*/
		y.x, y.y, y.z, 0.0f, /* 2. col*/
		z.x, z.y, z.z, 0.0f, /* 3. col*/
		position.x, position.y, position.z, 1.0f /* 4. col*/
	);
	return matrix;
}

void CurveAnimation::alignObject(const vec3& position, const vec3& dir, const vec3& baseDir, Transform& transform) {
	transform.position = position;
	vec3 axis = cross(baseDir, dir);
	float a = orientedAngle(baseDir, normalize(dir), axis);
	if (a != 0) {
		transform.rotation = rotate(mat4(1.0f), a, axis);
	}
}

/* 	q(t) = 0.5 *[t ^ 3 t ^ 2 t 1] * [-1  3 -3  1] * [p1]
									[ 2 -5  4 -1]   [p2]
									[-1  0  1  0]   [p3]
									[ 0  2  0  0]   [p4]  */
vec3 CurveAnimation::evaluateCurveSegment(const vec3& P0, const vec3& P1, const vec3& P2, const vec3& P3, const float t) {
	vec4 time(t * t * t, t * t, t, 1);
	time *= 0.5f;
	vec4 tmp(
		time.x * -1.0f + time.y * 2.0f + time.z * -1.0f + time.w * 0.0f,
		time.x * 3.0f + time.y * -5.0f + time.z * 0.0f + time.w * 2.0f,
		time.x * -3.0f + time.y * 4.0f + time.z * 1.0f + time.w * 0.0f,
		time.x * 1.0f + time.y * -1.0f + time.z * 0.0f + time.w * 0.0f
	);
	return P0 * tmp.x + P1 * tmp.y + P2 * tmp.z + P3 * tmp.w;
}

vec3 CurveAnimation::evaluateCurveSegment_1stDerivative(const vec3& P0, const vec3& P1, const vec3& P2, const vec3& P3, const float t) {
	vec4 time(3.0f * t * t, 2.0f * t, 1.0f, 0.0f);
	time *= 0.5f;
	vec4 tmp(
		time.x * -1.0f + time.y * 2.0f + time.z * -1.0f + time.w * 0.0f,
		time.x * 3.0f + time.y * -5.0f + time.z * 0.0f + time.w * 2.0f,
		time.x * -3.0f + time.y * 4.0f + time.z * 1.0f + time.w * 0.0f,
		time.x * 1.0f + time.y * -1.0f + time.z * 0.0f + time.w * 0.0f
	);
	return P0 * tmp.x + P1 * tmp.y + P2 * tmp.z + P3 * tmp.w;
}

vec3 CurveAnimation::evaluateClosedCurve(const vec3 points[], const size_t count, const float t) {
	size_t i = int(t);
	return evaluateCurveSegment(
		points[(i + count - 1) % count],
		points[(i + 0) % count],
		points[(i + 1) % count],
		points[(i + 2) % count],
		t - i
	);
}

vec3 CurveAnimation::evaluateClosedCurve_1stDerivative(const vec3 points[], const size_t count, const float t) {
	size_t i = int(t);
	return evaluateCurveSegment_1stDerivative(
		points[(i + count - 1) % count],
		points[(i + 0) % count],
		points[(i + 1) % count],
		points[(i + 2) % count],
		t - i
	);
}
