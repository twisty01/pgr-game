#version 140

in vec2 uv;
out vec3 _uv;

uniform mat4 invPV;

void main()
{
    vec4 fp = vec4(uv, 0.9999, 1.0);
    vec4 worldViewCoord = invPV * fp;
    _uv = worldViewCoord.xyz / worldViewCoord.w;
    gl_Position = fp;
}  